package com.espressif.iot.device.builder;


import com.espressif.iot.device.IEspDevicePlugXq;
import com.espressif.iot.model.device.EspDevicePlugXq;

public class BEspDevicePlugXq implements IBEspDevicePlugXq
{
    private BEspDevicePlugXq()
    {
    }
    
    private static class InstanceHolder
    {
        static BEspDevicePlugXq instance = new BEspDevicePlugXq();
    }
    
    public static BEspDevicePlugXq getInstance()
    {
        return InstanceHolder.instance;
    }
    
    @Override
    public IEspDevicePlugXq alloc()
    {
        return  new  EspDevicePlugXq();
    }
    
}
