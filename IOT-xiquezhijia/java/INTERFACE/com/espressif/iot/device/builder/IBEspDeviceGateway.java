package com.espressif.iot.device.builder;


import com.espressif.iot.device.IEspDeviceGateway;
import com.espressif.iot.object.IEspObjectBuilder;

public interface IBEspDeviceGateway extends IEspObjectBuilder
{
	IEspDeviceGateway alloc();
}
