package com.espressif.iot.device.builder;

import com.espressif.iot.device.IEspDeviceCurtainXq;
import com.espressif.iot.model.device.EspDeviceCurtainXq;




public class BEspDeviceCurtainXq implements IBEspDeviceCurtainXq
{
    private BEspDeviceCurtainXq()
    {
    }
    
    private static class InstanceHolder
    {
        static BEspDeviceCurtainXq instance = new BEspDeviceCurtainXq();
    }
    
    public static BEspDeviceCurtainXq getInstance()
    {
        return InstanceHolder.instance;
    }
    
    @Override
    public IEspDeviceCurtainXq alloc()
    {
        return  new  EspDeviceCurtainXq();
    }
    
}
