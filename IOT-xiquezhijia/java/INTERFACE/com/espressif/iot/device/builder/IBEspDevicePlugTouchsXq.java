package com.espressif.iot.device.builder;



import com.espressif.iot.device.IEspDeviceXq;
import com.espressif.iot.object.IEspObjectBuilder;

public interface IBEspDevicePlugTouchsXq extends IEspObjectBuilder
{
	IEspDeviceXq alloc();
}
