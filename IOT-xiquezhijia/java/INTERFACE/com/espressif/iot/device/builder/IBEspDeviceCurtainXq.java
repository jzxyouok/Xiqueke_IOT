package com.espressif.iot.device.builder;



import com.espressif.iot.device.IEspDeviceCurtainXq;
import com.espressif.iot.object.IEspObjectBuilder;

public interface IBEspDeviceCurtainXq extends IEspObjectBuilder
{
	IEspDeviceCurtainXq alloc();
}
