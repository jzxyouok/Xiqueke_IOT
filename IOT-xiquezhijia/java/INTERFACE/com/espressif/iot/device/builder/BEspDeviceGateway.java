package com.espressif.iot.device.builder;

import com.espressif.iot.device.IEspDeviceGateway;
import com.espressif.iot.device.IEspDeviceLight;
import com.espressif.iot.model.device.EspDeviceGateway;

public class BEspDeviceGateway implements IBEspDeviceGateway
{
    private BEspDeviceGateway()
    {
    }
    
    private static class InstanceHolder
    {
        static BEspDeviceGateway instance = new BEspDeviceGateway();
    }
    
    public static BEspDeviceGateway getInstance()
    {
        return InstanceHolder.instance;
    }
    
    @Override
    public IEspDeviceGateway alloc()
    {
        return  new  EspDeviceGateway();
    }
    
}
