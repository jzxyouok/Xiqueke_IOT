package com.espressif.iot.device.builder;


import com.espressif.iot.device.IEspDevicePlugXq;
import com.espressif.iot.object.IEspObjectBuilder;

public interface IBEspDevicePlugXq extends IEspObjectBuilder
{
	IEspDevicePlugXq alloc();
}
