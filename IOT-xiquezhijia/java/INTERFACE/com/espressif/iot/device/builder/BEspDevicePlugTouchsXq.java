package com.espressif.iot.device.builder;

import com.espressif.iot.device.IEspDeviceXq;
import com.espressif.iot.model.device.EspDevicePlugTouchsXq;




public class BEspDevicePlugTouchsXq implements IBEspDevicePlugTouchsXq
{
    private BEspDevicePlugTouchsXq()
    {
    }
    
    private static class InstanceHolder
    {
        static BEspDevicePlugTouchsXq instance = new BEspDevicePlugTouchsXq();
    }
    
    public static BEspDevicePlugTouchsXq getInstance()
    {
        return InstanceHolder.instance;
    }
    
    @Override
    public IEspDeviceXq alloc()
    {
        return  new  EspDevicePlugTouchsXq();
    }
    
}
