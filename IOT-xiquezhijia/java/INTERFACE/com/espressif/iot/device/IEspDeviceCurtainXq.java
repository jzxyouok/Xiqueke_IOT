package com.espressif.iot.device;


import com.espressif.iot.type.device.status.IEspStatusCurtain;


public interface IEspDeviceCurtainXq extends IEspDevice
{

    
    /**
     * Get the status of the plug
     * 
     * @return the status @see IEspStatusPlug
     */
    IEspStatusCurtain getStatusCurtain();
    
    /**
     * Set the status of the plug
     * 
     * @param statusPlug @see IEspStatusPlug
     */
    void setStatusCurtain(IEspStatusCurtain statuscurtain);
   
}
