package com.espressif.iot.device;



public interface IEspDeviceTouchsXq 
{

    int getPlugtouchnum();
    
    /**
     * Set the number of the plug
     * 
     * @return the number @see 
     */
    void setPlugtouchnum(int num);
    
    /**
     * Get the total number of the plug
     * 
     * @return the number @see 
     */
    int getPlugtouchtotalnum();
    
    /**
     * Set the total number of the plug
     * 
     * @return the number @see 
     */
    void setPlugtouchtotalnum(int num);
}
