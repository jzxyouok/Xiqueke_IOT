package com.espressif.iot.device;

import java.net.InetAddress;

import com.espressif.iot.type.device.status.IEspStatusGateway;

public interface IEspDeviceGateway extends IEspDevice
{

    
    /**
     * Get the status of the light
     * 
     * @return the status @see IEspStatusLight
     */
    IEspStatusGateway getStatusGateway();
    
    /**
     * Set the status of the light
     * 
     * @param statusLight @see IEspStatusLight
     */
    void setStatusGateway(IEspStatusGateway statusLight);
    /**
     * Get the inetaddress of gateway
     * 
     * @param 
     */
    InetAddress GetInetAddress();
    /**
     * Set the inetaddress of gateway
     * 
     * @param statusLight @see IEspStatusLight
     */
    void SetInetAddress(InetAddress host);

	
}
