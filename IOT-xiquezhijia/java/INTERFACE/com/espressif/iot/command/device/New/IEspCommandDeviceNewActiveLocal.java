package com.espressif.iot.command.device.New;

import com.espressif.iot.command.IEspCommandInternet;
import com.espressif.iot.command.device.IEspCommandActivated;
import com.espressif.iot.command.device.IEspCommandNew;
import com.espressif.iot.device.IEspDevice;
import com.espressif.iot.model.device.EspDeviceGateway;

public interface IEspCommandDeviceNewActiveLocal extends IEspCommandNew, IEspCommandActivated, IEspCommandInternet
{
//    static final String URL = "https://iot.espressif.cn/v1/key/authorize/?query_devices_mesh=true";
    
  
    /**
     * Activate the new device online or local //xiquekeji  iot   device
     *   the deviceKey is gateway owner
     * @param devicekey
     * @return the new add device
     */
    IEspDevice doCommandNewActiveLocal(String deviceKey);
    
    /**
     * Activate the new device online or local //xiquekeji  iot   device
     *   the deviceKey is childdevice owner
     * @param devicekey
     * @return the new add device
     */
    boolean doCommandNewChildActiveLocal(String deviceKey, EspDeviceGateway gateway);    
}
