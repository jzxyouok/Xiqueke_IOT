package com.espressif.iot.command.device;

import com.espressif.iot.command.IEspCommand;

/**
 * IEspCommandDevice indicate that the command is belong to devices
 * 
 * @author afunx
 * 
 */
public interface IEspCommandDevice extends IEspCommand
{
	 enum Frame_index{
		 START_BYTE1, START_BYTE2, UID_H, UID_L, 
		 FRAME_LENGTH, COMMD_TYPE, MACADDR1,
		 MACADDR2,MACADDR3,MACADDR4,MACADDR5,MACADDR6,
		 MACADDR7,MACADDR8,DATA,
	 }
	 enum ResPonse_type{
		 Set_OK(0x80), Dev_status(0x81),Dev_add_OK(0x82),
		 Sync_time_OK(0x83), Update_ready_OK(0x84),
		 GatetoSer_heartbeat(0x85), GatetoPhone_hearbeat(0x86),
		 Gate_ReqSer(0x87),GatetoPhone_addtime_OK(0x88),
		 Gate_report_Mac(0x89), GatetoPhone_deltime_OK(0x8A),
		 GatetoPhoe_devinfo(0x8B),Gatetophone_modifytime_OK(0x8c),
		 GatetoPhone_searlink_OK(0x8D), Gatetophone_Curtain_status(0x8E),
		 GatetoPhone_doorlock_status(0x8F);
		 
		 public byte mCode;
		 private ResPonse_type(int _mCode){
			 this.mCode = (byte)_mCode;
		 }
		 
	 }
	 enum Command_type{
		Set_DEV(0), Get_DEV(1), Edit_Devlist(2), Sync_time(3),
		Update_firmware(4), SG_Heartbeat(5), Phone_Heartbeat(6),
		S_res_loginok(7), Phone_add_time(8), Phone_getGateMac(9),
		Phone_del_time(0x0A), Get_devinfo(0x0B), Phone_modify_time(0x0C),
		Phone_searlink(0x0D), Phone_Set_Curtain(0x0E), PhonetoGate_Set_doorlock(0x0F);
		public byte nCode;
		private Command_type(int _nCode)
		{
			this.nCode = (byte)_nCode;
		}		
	 }
	 	
	 
	 static final byte FE = (byte)0xfe;
	    
	 static final byte E7 = (byte)0xe7;
	 /* FE	E7	XX	XX	0B	00	0x0101000000000001	11	校验和
	  * */
	 static final int PLUG_CTRL_LENGHT = 16;
	 
	 static final int PLUG_CTRL_LENGHT_RET = 16;
	 
	 static final int PLUG_LIGHT_CTRL_LENGHT = 17;
	 
	 static final int PLUG_LIGHT_CTRL_LENGHT_RET = 17;
	 
	 static final int PLUG_GETINFO_LENGHT = 15; 
	 
	 static final int CURTAIN_CTRL_LENGHT = 16;
	 
	 static final int CURTAIN_CTRL_LENGHT_RET = 16; 
	 
	 static final int DOORLOCK_CTRL_LENGHT = 16;
	 
	 static final int DOORLOCK_CTRL_LENGHT_RET = 16; 
	 
	 static final int DOORLOCK_GETSTATUS_LENGHT = 15;
	 static final int DOORLOCK_STATUS_LENGHT_RET = 16;
	 
	 static final int IRELECTRI_GETSTATUS_LENGHT = 15;
	 static final int IRELECTRI_STATUS_LENGHT_RET = 16;
	 
	 static final int FLAMMABLE_GETSTATUS_LENGHT = 15;
	 static final int FLAMMABLE_STATUS_LENGHT_RET = 16;	 
	 
	 static final int IRELECGRID_Alarm_LENGHT = 16;
	 static final int IRELECGRID_Alarm_LENGHT_RET = 15;
	 
	 static final int DOORmagnet_Alarm_LENGHT = 16;
	 static final int DOORmagnet_Alarm_LENGHT_RET = 16;
	 
	  
}
