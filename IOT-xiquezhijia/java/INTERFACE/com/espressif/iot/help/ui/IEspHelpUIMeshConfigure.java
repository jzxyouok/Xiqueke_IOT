package com.espressif.iot.help.ui;

public interface IEspHelpUIMeshConfigure extends IEspHelpUI
{
    void onHelpMeshConfigure();
}
