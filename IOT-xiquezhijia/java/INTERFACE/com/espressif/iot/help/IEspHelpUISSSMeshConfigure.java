package com.espressif.iot.help;

import com.espressif.iot.help.ui.IEspHelpUI;

public interface IEspHelpUISSSMeshConfigure extends IEspHelpUI
{
    void onHelpSSSMeshConfigure();
}
