package com.espressif.iot.object.db;

import java.util.List;

import com.espressif.iot.db.greenrobot.daos.XqzoneDB;
import com.espressif.iot.object.IEspDBManager;

public interface IXqzoneDBManager extends IEspDBManager
{
   
	
	void insertOrReplace(int iconid, String zonename);
	/**
     * insert or replace the device in local db
     * 
     * @param zoneId zone's id
     * @param iconid zone's icon
    
     */
    void insertOrReplace(long zoneId, int iconid, String zonename);
    
    /**
     * delete the zone in local db
     * 
     * @param zoneId the zone's id
     */
    void delete(long zoneId);
    
    void delete(String name);


    /**
     * rename the zone in local db if the device exist
     * @param zoneId the zone's id
     * @param name the zone's new name
     */
    void renameDevice(long zoneId,String name);

	List<XqzoneDB> getAllXqzoneDBList();
	
	XqzoneDB getzoneDB(String name);
}
