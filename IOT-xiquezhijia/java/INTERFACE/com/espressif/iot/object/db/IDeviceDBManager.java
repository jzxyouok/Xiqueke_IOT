package com.espressif.iot.object.db;

import java.util.List;

import com.espressif.iot.db.greenrobot.daos.DeviceDB;
import com.espressif.iot.object.IEspDBManager;

public interface IDeviceDBManager extends IEspDBManager
{
    /**
     * insert or replace the device in local db
     * 
     * @param deviceId device's id
     * @param key device's key(activated) or device' temp random40 key(activating)
     * @param bssid device's bssid(sta)
     * @param type device's type
     * @param state device's state
     * @param isOwner whether the user is the device owner
     * @param name device's name
     * @param rom_version the current sdk rom version
     * @param latest_rom_version the latest sdk rom version
     * @param timestamp the UTC millisecond timestamp when the device is activated
     * @param userId the user's id
     */
    void insertOrReplace(long deviceId, String key, String bssid, int type, int state, boolean isOwner, String name,
        String rom_version, String latest_rom_version, long timestamp, long userId, long zoneid);
    
    /**
     * delete the device in local db
     * 
     * @param deviceId the device's id
     */
    void delete(long deviceId);
    
    /**
     * insert the activating device
     * 
     * @param key the temp random40 key
     * @param bssid the device's bssid(sta)
     * @param type the device's type
     * @param state the device's state
     * @param name the device's name
     * @param rom_version the current sdk rom version
     * @param latest_rom_version the latest sdk rom version
     * @param timestamp the timestamp the device is activating
     * @param userId the user id
     * @return the device id
     */
    long insertActivatingDevice(String key, String bssid, int type, int state, String name, String rom_version,
        String latest_rom_version, long timestamp, long userId, long zoneid);

    /**
     * rename the device in local db if the device exist
     * @param deviceId the deivce's id
     * @param name the device's new name
     */
    void renameDevice(long deviceId,String name);
    
    /* reload the device list
     * 
     * */
    List<DeviceDB> getAllDevice();
    /* reload the device list belong to the zoneid
     * 
     * */
    List<DeviceDB> getDevicesofZone(long zoneid);
    /**
     *  load the gatelist from devicedb
     * */
    List<DeviceDB> getGatewayDevices();
}
