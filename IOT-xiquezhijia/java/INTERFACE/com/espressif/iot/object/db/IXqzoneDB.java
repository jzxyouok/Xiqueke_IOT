package com.espressif.iot.object.db;

import com.espressif.iot.object.IEspDBObject;

/**
 * the Device db should save such properties as follows
 * 
 * @author afunx
 * 
 */
public interface IXqzoneDB extends IEspDBObject
{
		/*	get zone id
		 * 
		 * */
	 	public Long getId();
	 	/*	set zone id
		 * 
		 * */
	    public void setId(Long id);
	    /*	get child count
		 * 
		 * */
	    public Integer getChildcount();
	    /*	set zone id
		 * 
		 * */
	    public void setChildcount(Integer childcount);
	    /*	get icon id
		 * 
		 * */
	    public Integer getIconid();

	    public void setIconid(Integer iconid);
	    
	    /*	set zone name
		 * 
		 * */
	    public String getXqzoneName();
	    public void setXqzoneName(String xqzoneName);

    
}
