package com.espressif.iot.type.device.status;

import com.espressif.iot.type.device.IEspDeviceStatus;

public interface IEspStatusCurtain extends IEspDeviceStatus
{
    /**
     * Check whether the plug is on off or pause
     * 0  --off
     * 1----on
     * 2----pause
     * @return whether the plug is on
     */
    int isOn();
    
    /**
     * Set whether the plug is on off or pause
     * 
     * @param isOn whether the plug is on
     */
    void setIsOn(int isOn);
}
