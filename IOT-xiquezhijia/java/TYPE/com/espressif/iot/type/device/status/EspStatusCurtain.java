package com.espressif.iot.type.device.status;



public class EspStatusCurtain implements IEspStatusCurtain, Cloneable
{
    private int  mIsOn;
    // 1为开 2为关  0为停止
    @Override
    public int isOn()
    {
        return mIsOn;
    }
    
    @Override
    public void setIsOn(int isOn)
    {
        mIsOn = isOn;
    }
    
    @Override
    public String toString()
    {
        return "EspStatusCurtain: (mIsOn=[" + mIsOn + "])";
    }
    
    @Override
    public Object clone()
        throws CloneNotSupportedException
    {
        return super.clone();
    }
}
