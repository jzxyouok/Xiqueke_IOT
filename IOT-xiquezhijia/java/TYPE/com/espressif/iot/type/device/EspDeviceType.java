package com.espressif.iot.type.device;

import com.mob.tools.utils.HEX;

/**
 * NOTE: use {@link #toString} instead of {@link #name()} use {@link #getSerial()} instead of {@link #ordinal()}
 * 
 * @author afunx
 * 
 */
public enum EspDeviceType
{
    
    NEW(-1, "New", true),
    ROOT(-2, "Root Router", false),
    PLUG(23701, "Plug", true),
    LIGHT(45772, "Light", true),
    HUMITURE(12335, "Humiture", false),
    FLAMMABLE(3835, "Flammable Gas", false),
    VOLTAGE(68574, "Voltage", false),
    REMOTE(2355, "Remote", true),
    PLUGS(47446, "Plugs", true),
    GATEWAY(1, "Gateway", true),
    PLUGTOUCH(0x101, "Plugtouch", true),
    PLUGTOUCH_2(0x102, "Plugtouch2", true),
    PLUGTOUCH_3(0x103, "Plugtouch3", true),
    PLUGTOUCH_4(0x104, "Plugtouch4", true),
    PLUGTOUCH_LIGHT(0x501, "Plugtouchlight", true),
    PLUGTOUCH_LIGHT_2(0x601, "Plugtouchlight2", true),
    DOORLOCK(0x1201, "Doorlock", true),
    CURTAIN(0x0302, "Curtain", true),
    WIFICAMERA(0x1401, "WifiCamera", true),
    REMOTE_XQ(0x1301, "Remotexq", true),
    IRCONVERT(0x901, "Irconvert", true),
    PLUG_XQ(0x701, "Plugxq", true),
    PLUG_METER(0x801, "Plugmeter", true),
    FLAMMABLE_XQ(0x1101, "Flammablexq", true),
    WIFIPLUG_XQ(0X1501, "Wifiplugxq", true),
    WIFIPLUGS_XQ(0X1601, "Wifiplugsxq", true),
    WIFICONVERT(0X1701, "Wificonvert", true);
    private final int mSerial;
    
    private final String mString;
    
    private final boolean mIsLocalSupport;
    
    private EspDeviceType(int serial, String string, boolean isLocalSupport)
    {
        this.mSerial = serial;
        this.mString = string;
        this.mIsLocalSupport = isLocalSupport;
    }
    
    /**
     * Check whether the type of device support local
     * 
     * @return whether the type of device support local
     */
    public boolean isLocalSupport()
    {
        return this.mIsLocalSupport;
    }
    
    /**
     * Get the serial of the device type defined by Server
     * 
     * @return the serial of the device type defined by Server
     */
    public int getSerial()
    {
        return mSerial;
    }
    
    @Override
    public String toString()
    {
        return this.mString;
    }
    
    /**
     * Get EspDeviceType by its serial
     * 
     * @param serial the serial of the device type defined by Server
     * @return the EspDeviceType
     */
    public static EspDeviceType getEspTypeEnumBySerial(int serial)
    {
        if (serial == NEW.getSerial())
        {
            return NEW;
        }
        else if (serial == PLUG.getSerial())
        {
            return PLUG;
        }
        else if (serial == LIGHT.getSerial())
        {
            return LIGHT;
        }
        else if (serial == HUMITURE.getSerial())
        {
            return HUMITURE;
        }
        else if (serial == FLAMMABLE.getSerial())
        {
            return FLAMMABLE;
        }
        else if (serial == VOLTAGE.getSerial())
        {
            return VOLTAGE;
        }
        else if (serial == REMOTE.getSerial())
        {
            return REMOTE;
        }
        else if (serial == ROOT.getSerial())
        {
            return ROOT;
        }
        else if (serial == PLUGS.getSerial())
        {
            return PLUGS;
        }
        else if (serial == GATEWAY.getSerial())
        {
            return GATEWAY;
        }
        else if(serial == PLUG_XQ.getSerial())
        {
        	return PLUG_XQ;
        }
        else if(serial == PLUGTOUCH.getSerial())
        {
        	return PLUGTOUCH;
        }
        else if(serial == PLUGTOUCH_2.getSerial())
        {
        	return PLUGTOUCH_2;
        }
        else if(serial == PLUGTOUCH_3.getSerial())
        {
        	return PLUGTOUCH_3;
        }
        else if(serial == PLUGTOUCH_4.getSerial())
        {
        	return PLUGTOUCH_4;
        }
        else if(serial == PLUGTOUCH_4.getSerial())
        {
        	return PLUGTOUCH_4;
        }
        else if(serial == CURTAIN.getSerial())
        {
        	return CURTAIN;
        }
        return null;
    }
    
    /**
     * Get EspDeviceType by its typeEnum String
     * 
     * @param typeEnumString the type enum String of the device
     * @return the EspDeviceType
     */
    public static EspDeviceType getEspTypeEnumByString(String typeEnumString)
    {
        if (typeEnumString.equals(EspDeviceType.NEW.toString()))
        {
            return NEW;
        }
        else if (typeEnumString.equals(EspDeviceType.PLUG.toString()))
        {
            return PLUG;
        }
        else if (typeEnumString.equals(EspDeviceType.LIGHT.toString()))
        {
            return LIGHT;
        }
        else if (typeEnumString.equals(EspDeviceType.HUMITURE.toString()))
        {
            return HUMITURE;
        }
        else if (typeEnumString.equals(EspDeviceType.FLAMMABLE.toString()))
        {
            return FLAMMABLE;
        }
        else if (typeEnumString.equals(EspDeviceType.VOLTAGE.toString()))
        {
            return VOLTAGE;
        }
        else if (typeEnumString.equals(EspDeviceType.REMOTE.toString()))
        {
            return REMOTE;
        }
        else if (typeEnumString.equals(EspDeviceType.ROOT.toString()))
        {
            return ROOT;
        }
        else if (typeEnumString.equals(EspDeviceType.PLUGS.toString()))
        {
            return PLUGS;
        }
        else if(typeEnumString.equals(EspDeviceType.GATEWAY.toString()))
        {
        	return GATEWAY;
        }
        return null;
    }
    
    //检查是否为xiquekeji SMartDevice
    /*  like the getEspTypeEnumBySerial 
     * */
    public static EspDeviceType getDevicetypte(String sharedDeviceKey) {
		// TODO Auto-generated method stub
		String deviceid =null;
		deviceid = sharedDeviceKey.substring(2, 6);
		byte[] deviceserial = HEX.decodeHexString(deviceid);
		int xique_PD = deviceserial[0]*256+deviceserial[1];
		if(xique_PD == GATEWAY.getSerial()){
			return GATEWAY;
		}else if(xique_PD == PLUGTOUCH.getSerial()){
			return PLUGTOUCH;
		}else if(xique_PD == PLUGTOUCH_2.getSerial()){
			return PLUGTOUCH_2;
		}
	    else if(xique_PD == PLUGTOUCH_3.getSerial()){
	    	return PLUGTOUCH_3;
		}
	    else if(xique_PD == PLUGTOUCH_4.getSerial()){
	    	return PLUGTOUCH_4;
		}
	    else if(xique_PD == PLUGTOUCH_LIGHT.getSerial()){
	    	return PLUGTOUCH_LIGHT;
		}
	    else if(xique_PD == PLUGTOUCH_LIGHT_2.getSerial()){
	    	return PLUGTOUCH_LIGHT_2;
		}else if(xique_PD == DOORLOCK.getSerial()){
			return DOORLOCK;
		}else if(xique_PD == CURTAIN.getSerial()){
			return CURTAIN;
		}else if(xique_PD == WIFICAMERA.getSerial()){
			return WIFICAMERA;
		}else if(xique_PD == REMOTE_XQ.getSerial()){
			return REMOTE_XQ;
		}else if(xique_PD == IRCONVERT.getSerial()){
			return IRCONVERT;
		}else if(xique_PD == PLUG_XQ.getSerial()){
			return PLUG_XQ;
		}else if(xique_PD == PLUG_METER.getSerial()){
			return PLUG_METER;
		}else if(xique_PD == FLAMMABLE_XQ.getSerial()){
			return FLAMMABLE_XQ;
		}
    
		return null;
	}
    
    public static boolean XQchildDeviceisvalid(EspDeviceType type){
	
    	switch(type){
    	case PLUGTOUCH:
    	case PLUGTOUCH_2:
    	case PLUGTOUCH_3:
    	case PLUGTOUCH_4:
    	case PLUGTOUCH_LIGHT:
    	case PLUGTOUCH_LIGHT_2:
    	case DOORLOCK:
    	case CURTAIN:
    	//case WIFICAMERA:
    	case REMOTE_XQ:
    	case IRCONVERT:
    	case PLUG_XQ:
    	case PLUG_METER:
    	case FLAMMABLE_XQ:
    		return true;
    	default :
    		return false;
    	}
		 
    }
}
