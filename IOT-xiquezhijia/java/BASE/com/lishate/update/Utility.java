package com.lishate.update;

import com.lishate.utility.GobalDef;



public class Utility {
//�жϰ汾�Ƿ�������ص��汾��Ϣ  �ж��Ƿ�ӷ��������ش�update.xml
	public static boolean GetServerVersionInfo(){
		String path = GobalDef.Instance.getCachePath() + "/" + GobalDef.UPDATE_XML_NAME;
		return com.lishate.utility.Utility.HttpDownload(GobalDef.UPDATE_URL, path);
	}
	
	public static boolean CheckServerVersionUpdate(){
		if(GetServerVersionInfo() == true){
			UpdateInfo ui = UpdateInfo.getUpdateInfo();
			if(ui != null){
				return ui.checkIsUpdate();
			}
		}
		return false;
	}
	
	public static VersionItem GetVersionItem(){
		if(GetServerVersionInfo() == true){
			UpdateInfo ui = UpdateInfo.getUpdateInfo();
			if(ui != null){
				return ui.GetLastSuitableVersion();
			}
		}
		return null;
	}
}
