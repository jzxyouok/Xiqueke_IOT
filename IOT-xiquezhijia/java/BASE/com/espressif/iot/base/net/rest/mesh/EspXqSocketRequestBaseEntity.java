package com.espressif.iot.base.net.rest.mesh;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class EspXqSocketRequestBaseEntity implements IEspSocketRequest
{

    


    private final String mOriginUrl;

    private final String mHost;
    private final String mContent;

   
    private final String mRouter;
    
    /**
     * Constructor of EspSocketRequestBaseEntity
     * @param method the method
     * @param uriStr the String of URI
     * @param content the content
     * @param router
     */
    public EspXqSocketRequestBaseEntity(String uriStr,String content,String router)
    {
        //this.mMethod = method;
        this.mContent = content;
        this.mRouter = router;
        this.mOriginUrl = uriStr;
        
       
        // parse URI
        URI uri = URI.create(uriStr);
        System.out.println("EspXqSocketRequestBaseEntity uri:" + uri);
        this.mHost = uri.getHost();
    }
    
    /**
     * Constructor of EspSocketRequestBaseEntity
     * @param method the method
     * @param uriStr the String of URI
     * @param content the content
     */
    public EspXqSocketRequestBaseEntity(String uriStr, String content)
    {
        this(uriStr, content, null);
    }
    
 
    




    
    /**
     * Get the host of the uri
     * @return the host of the uri
     */
    public String getHost(){
        return this.mHost;
    }
    
    @Override
    public String getContent()
    {
        return this.mContent;
    }
    
    @Override
    public String getRouter()
    {
        return this.mRouter;
    }
    
    @Override
    public String getOriginUri()
    {
        return this.mOriginUrl;
    }
    /*
     * 这里只需要把我们的数组转换为字符串就行了
     */
    
    @Override
    public String toString()
    {
        
        return mContent;
    }

	@Override
	public void putHeaderParams(String key, String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getRelativeUrl() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getScheme() {
		// TODO Auto-generated method stub
		return null;
	}
}
