package com.espressif.iot.base.net.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import android.util.Log;

import com.espressif.iot.type.device.EspDeviceType;
import com.espressif.iot.type.net.IOTAddress;

public class UdpBroadcastUtil
{
    
    private static final Logger log = Logger.getLogger(UdpBroadcastUtil.class);
    
    /* gateway can answer etc:"192.168.1.XXX，AB5C2E158A964D6F,"
     * */
    private static final String data = "are you xique PD?";//Are You Espressif IOT Smart Device?";
    
    private static final int IOT_DEVICE_PORT = 48899;//1025;
    
    private static final int SO_TIMEOUT = 10000;
    
    private static final int RECEIVE_LEN = 64;
    
    /**
     * if the IOT_APP_PORT is occupied, other random port will be used
     */
    private static final int IOT_APP_PORT =48899;//;4025 
    
    private static InetAddress broadcastAddress;
    
    static
    {
        try
        {
            broadcastAddress = InetAddress.getByName("255.255.255.255");
        }
        catch (UnknownHostException e)
        {
            e.printStackTrace();
        }
    }
    
    /**
     * alloc the port number
     * 
     * @param hostPort if -1<port<65536, it will allocate a random port, which could be used else it will allocate the
     *            specified port first, after that it will allocate a random one
     * @return DatagramSocket
     */
    private static DatagramSocket allocPort(int hostPort)
    {
        
        log.debug("allocPort() entrance");
        
        boolean success = false;
        DatagramSocket socket = null;
        
        // try to allocate the specified port
        if (-1 < hostPort && hostPort < 65536)
        {
            try
            {
            	/*Constructs a UDP datagram socket which is bound to 
            	 * the specific port aPort on the localhost. 
            	 * Valid values for aPort are between 0 and 65535 inclusive.
            	 */
                socket = new DatagramSocket(hostPort);
                success = true;
                log.debug(Thread.currentThread().toString() + "##allocPort(hostPort=[" + hostPort + "]) suc");
                return socket;
            }
            catch (SocketException e)
            {
                log.error(Thread.currentThread().toString() + "##allocPort(hostPort=[" + hostPort + "]) is used");
            }
        }
        // allocate a random port
        do
        {
            try
            {
                // [1024,65535] is the dynamic port range
                hostPort = 1024 + new Random().nextInt(65536 - 1024);
                socket = new DatagramSocket(hostPort);
                success = true;
            }
            catch (SocketException e)
            {
                e.printStackTrace();
            }
        } while (!success);
        return socket;
    }
    
    private static List<IOTAddress> __discoverDevices(String bssid)
    {
        List<IOTAddress> responseList = new ArrayList<IOTAddress>();
        DatagramSocket socket = null;
        byte buf_receive[] = new byte[RECEIVE_LEN];
        DatagramPacket pack = null;
        String receiveContent = null;
        String hostname = null;
        boolean isMesh = false;
        InetAddress responseAddr = null;
        String responseBSSID = null;
        String realData = null;
        if (bssid != null)
        {
            realData = data + " " + bssid;
        }
        else
        {
            realData = data;
        }
        try
        {
            // alloc port for the socket
            socket = allocPort(IOT_APP_PORT);
            // set receive timeout
            socket.setSoTimeout(SO_TIMEOUT);
            /* broadcast content
             * Constructs a new DatagramPacket object to send data 
             * to the port aPort of the address host. 
             * The length must be lesser than or equal to the size of data. 
             * The first length bytes are sent.
             */
            pack = new DatagramPacket(realData.getBytes(), realData.length(), broadcastAddress, IOT_DEVICE_PORT);
            log.debug(Thread.currentThread().toString() + "##__discoverDevices(bssid=[" + bssid + "]): socket send");
            socket.send(pack);
            //Sets the data buffer for this datagram packet. 
            //The length of the datagram packet is set to the buffer length.
            pack.setData(buf_receive);
            long start = System.currentTimeMillis();
            log.debug(Thread.currentThread().toString() + "##__discoverDevices(bssid=[" + bssid
                + "]): socket receive...");
            
            do
            {
            	//Log.e("udp broadcast device", "----------------------------1111" + receiveContent);
            	socket.receive(pack);
                //Log.e("udp broadcast device", "----------------------------2222222" + receiveContent);
                long consume = System.currentTimeMillis() - start;
                log.error("udp receivce cost: " + consume + " ms");
                log.debug(Thread.currentThread().toString() + "##__discoverDevices(bssid=[" + bssid
                    + "]): one socket received");
                /* Gets the offset of the data stored in this datagram packet.
                 * 
                 * */
                receiveContent = new String(pack.getData(), pack.getOffset(), pack.getLength());
                
                Log.e("udp broadcast device", "receiveContent =" + receiveContent);
                log.error("rev data isvalid:  " + UdpDataParser.isValid(receiveContent));
                if (UdpDataParser.isValid(receiveContent))
                {

                    EspDeviceType deviceType = EspDeviceType.getEspTypeEnumByString("Gateway");
                    Log.e("udp broadcast device", "receiveContent =" + receiveContent);
                    if (deviceType == null )//|| !deviceType.isLocalSupport())
                    {
                        log.warn(Thread.currentThread().toString() + "##__discoverDevices(bssid=[" + bssid
                            + "]): type is null or the type of the device don't support local mode.");
                    }
                    else
                    {
                        hostname = UdpDataParser.filterIpAddress(receiveContent);
                        bssid = UdpDataParser.filterBssid(receiveContent);
                        log.error(Thread.currentThread().toString() + "##__discoverDevices(bssid=[" + bssid
                            + "]): hostname=" + hostname);
                        responseAddr = InetAddress.getByName(hostname);
                        log.debug(receiveContent);
                        

                        IOTAddress iotAddress = new IOTAddress(bssid, responseAddr);
                        iotAddress.setEspDeviceTypeEnum(deviceType);
                        responseList.add(iotAddress);
                    }
                }
            } while (bssid == null);
        }
        catch (SocketException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // ignore SocketTimeoutException
            if (e instanceof SocketTimeoutException)
            {
                
            }
            else
            {
                e.printStackTrace();
            }
        }
        finally
        {
            if (socket != null)
            {
                socket.disconnect();
                socket.close();
                log.info(Thread.currentThread().toString() + "##__discoverDevices(bssid=[" + bssid
                    + "]): socket is not null, closed in finally");
            }
            else
            {
                log.warn(Thread.currentThread().toString() + "##__discoverDevices(bssid=[" + bssid
                    + "]): sockect is null, closed in finally");
            }
        }
        //这里有正常返回网关信息
        return responseList;
    }
    
    /**
     * @see IOTAddress discover the specified IOT device in the same AP by UDP broadcast
     * 
     * @param bssid the IOT device's bssid
     * @return the specified device's IOTAddress (if found) or null(if not found)
     */
    public static IOTAddress discoverIOTDevice(String bssid)
    {
        List<IOTAddress> result = __discoverDevices(bssid);
        if (!result.isEmpty())
        {
            return result.get(0);
        }
        else
        {
            return null;
        }
    }
    
    /**
     * @see IOTAddress discover IOT devices in the same AP by UDP broadcast
     * 
     * @return the List of IOTAddress
     */
    public static List<IOTAddress> discoverIOTDevices()
    {
        List<IOTAddress> result = __discoverDevices(null);
        if (result != null)
        {
            return result;
        }
        else
        {
        	//Returns a type-safe empty, immutable List.
        	return Collections.emptyList();
        }
    }
    
}
