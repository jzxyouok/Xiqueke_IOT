package com.espressif.iot.db.greenrobot.daos;

import java.util.List;

import com.espressif.iot.db.greenrobot.daos.DaoSession;
import com.espressif.iot.object.db.IXqzoneDB;

import de.greenrobot.dao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END
/**
 * Entity mapped to table XQZONE_DB.
 */
public class XqzoneDB implements IXqzoneDB{

    private Long id;
    private Integer childcount;
    private Integer iconid;
    private String xqzoneName;

    /** Used to resolve relations */
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    private transient XqzoneDBDao myDao;

    private List<DeviceDB> devicelist;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    public XqzoneDB() {
    }

    public XqzoneDB(Long id) {
        this.id = id;
    }

    public XqzoneDB(Long id, Integer childcount, Integer iconid, String xqzoneName) {
        this.id = id;
        this.childcount = childcount;
        this.iconid = iconid;
        this.xqzoneName = xqzoneName;
    }
    
    public XqzoneDB(int iconid, String xqzonename){
    	this.iconid = iconid;
        this.xqzoneName = xqzonename;
    }

    /** called by internal mechanisms, do not call yourself. */
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getXqzoneDBDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getChildcount() {
        return childcount;
    }

    public void setChildcount(Integer childcount) {
        this.childcount = childcount;
    }

    public Integer getIconid() {
        return iconid;
    }

    public void setIconid(Integer iconid) {
        this.iconid = iconid;
    }

    public String getXqzoneName() {
        return xqzoneName;
    }

    public void setXqzoneName(String xqzoneName) {
        this.xqzoneName = xqzoneName;
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    public List<DeviceDB> getDevicelist() {
        if (devicelist == null) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            DeviceDBDao targetDao = daoSession.getDeviceDBDao();
            List<DeviceDB> devicelistNew = targetDao._queryXqzoneDB_Devicelist(id);
            synchronized (this) {
                if(devicelist == null) {
                    devicelist = devicelistNew;
                }
            }
        }
        return devicelist;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    public synchronized void resetDevicelist() {
        devicelist = null;
    }
    /** Convenient call for {@link AbstractDao#delete(Object)}. Entity must attached to an entity context. */
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.delete(this);
    }

    /** Convenient call for {@link AbstractDao#update(Object)}. Entity must attached to an entity context. */
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.update(this);
    }

    /** Convenient call for {@link AbstractDao#refresh(Object)}. Entity must attached to an entity context. */
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.refresh(this);
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
