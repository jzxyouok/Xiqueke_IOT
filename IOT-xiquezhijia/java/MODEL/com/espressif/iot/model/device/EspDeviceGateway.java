package com.espressif.iot.model.device;


import java.net.InetAddress;

import com.espressif.iot.db.greenrobot.daos.DeviceDB;
import com.espressif.iot.device.IEspDeviceGateway;
import com.espressif.iot.type.device.EspDeviceType;
import com.espressif.iot.type.device.state.EspDeviceState;
import com.espressif.iot.type.device.status.EspStatusGateway;
import com.espressif.iot.type.device.status.IEspStatusGateway;

public class EspDeviceGateway extends EspDevice implements IEspDeviceGateway
{
   

	private IEspStatusGateway mStatusGateway;
    private InetAddress       ipaddress;
    public EspDeviceGateway()
    {
        mStatusGateway = new EspStatusGateway();
    }
    public EspDeviceGateway(DeviceDB db){
    	this.mBssid = db.getBssid();
    	this.mDeviceId = Long.parseLong(db.getBssid());
    	this.mDeviceKey = db.getBssid();
    	this.mDeviceType = EspDeviceType.GATEWAY;
    	this.mDeviceName = db.getName();
    	this.mDeviceState = EspDeviceState.OFFLINE;
    	this.mIsMeshDevice = true;
    	
    	
    }
    @Override
    public Object clone()
        throws CloneNotSupportedException
    {
        EspDeviceGateway device = (EspDeviceGateway)super.clone();
        // deep copy
        IEspStatusGateway status = device.getStatusGateway();
        device.mStatusGateway = (IEspStatusGateway)((EspStatusGateway)status).clone();
        return device;
    }
    
    @Override
    public IEspStatusGateway getStatusGateway()
    {
        return mStatusGateway;
    }
    
    @Override
    public void setStatusGateway(IEspStatusGateway statusGateway)
    {
        mStatusGateway = statusGateway;
    }
    
    @Override
   	public void SetInetAddress(InetAddress host) {
   		// TODO Auto-generated method stub
    	ipaddress = host;
   	}
    @Override
    public InetAddress GetInetAddress(){
    	
		return ipaddress;
    	
    }
}
