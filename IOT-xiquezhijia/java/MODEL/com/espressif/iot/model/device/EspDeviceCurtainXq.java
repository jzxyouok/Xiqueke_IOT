package com.espressif.iot.model.device;




import com.espressif.iot.device.IEspDeviceCurtainXq;
import com.espressif.iot.type.device.status.EspStatusCurtain;
import com.espressif.iot.type.device.status.IEspStatusCurtain;

public class EspDeviceCurtainXq extends EspDevice implements IEspDeviceCurtainXq
		
{
    private IEspStatusCurtain mStatusCurtain;

    public EspDeviceCurtainXq()
    {
    	mStatusCurtain = new EspStatusCurtain();
    }
    
    @Override
    public Object clone()
        throws CloneNotSupportedException
    {
        EspDeviceCurtainXq device = (EspDeviceCurtainXq)super.clone();
        // deep copy
        IEspStatusCurtain status = device.getStatusCurtain();
        device.mStatusCurtain = (IEspStatusCurtain)((EspStatusCurtain)status).clone();
        return device;
    }
    
    
   

	@Override
	public IEspStatusCurtain getStatusCurtain() {
		// TODO Auto-generated method stub
		return this.mStatusCurtain;
	}

	@Override
	public void setStatusCurtain(IEspStatusCurtain statuscurtain) {
		// TODO Auto-generated method stub
		this.mStatusCurtain = statuscurtain;
	}


	
}
