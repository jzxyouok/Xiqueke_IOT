package com.espressif.iot.model.device;




import com.espressif.iot.device.IEspDeviceTouchsXq;
import com.espressif.iot.device.IEspDeviceXq;
import com.espressif.iot.type.device.status.EspStatusPlug;
import com.espressif.iot.type.device.status.IEspStatusPlug;

public class EspDevicePlugTouchsXq extends EspDevice implements IEspDeviceTouchsXq,
					IEspDeviceXq
		
{
    private IEspStatusPlug mStatusPlug;
    private int plug_num;
    private int total_plugnum;
    public EspDevicePlugTouchsXq()
    {
        mStatusPlug = new EspStatusPlug();
    }
    
    @Override
    public Object clone()
        throws CloneNotSupportedException
    {
        EspDevicePlugTouchsXq device = (EspDevicePlugTouchsXq)super.clone();
        // deep copy
        IEspStatusPlug status = device.getStatusPlug();
        device.mStatusPlug = (IEspStatusPlug)((EspStatusPlug)status).clone();
        return device;
    }
    
    
    @Override
    public IEspStatusPlug getStatusPlug()
    {
        return mStatusPlug;
    }
    
    @Override
    public void setStatusPlug(IEspStatusPlug statusPlug)
    {
        mStatusPlug = statusPlug;
    }


	@Override
	public int getPlugtouchnum() {
		// TODO Auto-generated method stub
		return this.plug_num;
	}

	@Override
	public void setPlugtouchnum(int num) {
		// TODO Auto-generated method stub
		this.plug_num = num;
	}

	@Override
	public int getPlugtouchtotalnum() {
		// TODO Auto-generated method stub
		return this.total_plugnum;
	}

	@Override
	public void setPlugtouchtotalnum(int num) {
		// TODO Auto-generated method stub
		this.total_plugnum = num;
	}
}
