package com.espressif.iot.model.device;


import com.espressif.iot.device.IEspDevicePlugXq;
import com.espressif.iot.type.device.status.EspStatusPlug;
import com.espressif.iot.type.device.status.IEspStatusPlug;

public class EspDevicePlugXq extends EspDevice implements IEspDevicePlugXq
{
    private IEspStatusPlug mStatusPlug;
    
    public EspDevicePlugXq()
    {
        mStatusPlug = new EspStatusPlug();
    }
    
    @Override
    public Object clone()
        throws CloneNotSupportedException
    {
        EspDevicePlugXq device = (EspDevicePlugXq)super.clone();
        // deep copy
        IEspStatusPlug status = device.getStatusPlug();
        device.mStatusPlug = (IEspStatusPlug)((EspStatusPlug)status).clone();
        return device;
    }
    
    @Override
    public IEspStatusPlug getStatusPlug()
    {
        return mStatusPlug;
    }
    
    @Override
    public void setStatusPlug(IEspStatusPlug statusPlug)
    {
        mStatusPlug = statusPlug;
    }
}
