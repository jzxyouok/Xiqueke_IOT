package com.espressif.iot.ui.device.timer;



import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Color;

import com.espressif.iot.type.device.timer.EspDeviceFixedTimeTimer;
import com.espressif.iot.type.device.timer.EspDeviceTimer;

public class DeviceLightFixedTimeTimerEditActivity extends DeviceTimerEditFixedTimerActivityAbs
{

    @Override
    protected String getEditAction()
    {
        //return mActionValues[mActionSpinner.getSelectedItemPosition()];

    	String lightpara = "";

    	lightpara = "set:r" + "=" + Color.red(color) 
    			 + "g"  + "=" + Color.green(color)
    			 + "b"  + "=" + Color.blue(color)
    			 + "b"  + "=" + bright
    			 + "p"  + "=" + 1000
    			 + "#";
    			
    			
    	return lightpara;
    }

    @Override
    protected String getTimerAction(EspDeviceTimer  timer)
    {
    	EspDeviceFixedTimeTimer mmtimer = (EspDeviceFixedTimeTimer)timer;
    	return mmtimer.getTimeAction().get(0).getAction();
    }

	
    
    

}
