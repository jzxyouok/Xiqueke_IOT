package com.espressif.iot.ui.device.timer;

import com.espressif.iot.device.IEspDevicePlugs;
import com.espressif.iot.type.device.timer.EspDeviceFixedTimeTimer;
import com.espressif.iot.type.device.timer.EspDeviceTimer;
import com.espressif.iot.util.EspStrings;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class DevicePlugsFixedTimeTimerEditActivity extends DeviceTimerEditFixedTimerActivityAbs
{
    private String mValue;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        mValue = mIntentBundle.getString(EspStrings.Key.DEVICE_TIMER_PLUGS_VALUE_KEY);
    }
    
    @Override
    protected String getEditAction()
    {
        return mActionValues[mActionSpinner.getSelectedItemPosition()] + mValue;
    }

    @Override
    protected String getTimerAction(EspDeviceTimer  timer)
    {
        String action = ((EspDeviceFixedTimeTimer)timer).getTimeAction().get(0).getAction();
        action = action.substring(0, action.length() - IEspDevicePlugs.TIMER_TAIL_LENGTH);
        return action;
    }

	
}
