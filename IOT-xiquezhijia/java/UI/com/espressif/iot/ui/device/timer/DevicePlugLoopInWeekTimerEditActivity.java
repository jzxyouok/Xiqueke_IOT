package com.espressif.iot.ui.device.timer;

import com.espressif.iot.type.device.timer.EspDeviceLoopWeekTimer;
import com.espressif.iot.type.device.timer.EspDeviceTimer;

public class DevicePlugLoopInWeekTimerEditActivity extends DeviceTimerEditLoopInWeekActivityAbs
{
    
    @Override
    protected String getEditAction()
    {
        return mActionValues[mActionSpinner.getSelectedItemPosition()];
    }
    
    @Override
    protected String getTimerAction(EspDeviceTimer  timer)
    {
        return ((EspDeviceLoopWeekTimer)timer).getTimeAction().get(0).getAction();
    }
    
}
