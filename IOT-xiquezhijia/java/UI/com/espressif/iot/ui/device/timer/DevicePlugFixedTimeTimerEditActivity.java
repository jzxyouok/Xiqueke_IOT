package com.espressif.iot.ui.device.timer;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.xiquezhijia.iot.R;
import com.espressif.iot.type.device.timer.EspDeviceFixedTimeTimer;
import com.espressif.iot.type.device.timer.EspDeviceTimer;

public class DevicePlugFixedTimeTimerEditActivity extends DeviceTimerEditFixedTimerActivityAbs
{
    
    @Override
    protected String getEditAction()
    {
        return mActionValues[mActionSpinner.getSelectedItemPosition()];
    	
    }

    @Override
    protected String getTimerAction(EspDeviceTimer  timer)
    {
        return ((EspDeviceFixedTimeTimer)timer).getTimeAction().get(0).getAction();
    }


    
    
    
   

    
    

}
