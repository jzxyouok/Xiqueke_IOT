package com.espressif.iot.ui.device.dialog;


import com.xiquezhijia.iot.R;
import com.espressif.iot.ui.view.EspColorPicker;
import com.espressif.iot.ui.view.EspColorPicker.OnColorChangedListener;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;

public  class ColorPlateDialog implements EspDeviceDialogInterface, DialogInterface.OnDismissListener,
    DialogInterface.OnCancelListener, android.view.View.OnClickListener, DialogInterface.OnClickListener, OnColorChangedListener, OnSeekBarChangeListener
{
   
    
    protected AlertDialog mDialog;
    protected Context mContext;
    protected int color;
    protected int bright;
    protected Button confirm;
    protected EspColorPicker colorpick;
    protected View colordisplay;
    protected CheckBox  light_onoff;
    protected CheckBox  colorreq;
    protected CheckBox  onoffreq;
    
    protected LinearLayout barcontainer;
    protected TextView     whitetext;
    protected SeekBar      whitebar;
    protected TextView     brightext;
    protected SeekBar	   brightbar;
    public ColorPlateDialog(Context context, int color, int bright)
    {
        mContext = context;
        this.color = color;
        this.bright = bright; 
        //requestonoff = false;
    }
    
    //覆写父类的方法
    /* 这是DialogInterface类的抽象方法
     * 供外部调用的，供实例化对象调用的
     * 
     * */
    @Override
    public void cancel()
    {
        if (mDialog != null)
        {
            mDialog.cancel();
        }
    }
    
    //覆写父类的方法
    //供外部调用的，供实例化对象调用的
    @Override
    public void dismiss()
    {
        if (mDialog != null)
        {
            mDialog.dismiss();
        }
    }

    //覆写父类的方法
    @Override
    public void show()
    {

        
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.colorplate_select,
		                               null);
		colordisplay = (View)layout.findViewById(R.id.light_color_display);
		
		colorpick = (EspColorPicker)layout.findViewById(R.id.light_color_picker);
		
		colorpick.setOnColorChangeListener(this);
		confirm = (Button)layout.findViewById(R.id.light_confirm_btn);
		light_onoff = (CheckBox)layout.findViewById(R.id.light_switch);
		light_onoff.setOnClickListener(this);
		light_onoff.setChecked(true);
		confirm.setOnClickListener(this);
		colorreq = (CheckBox)layout.findViewById(R.id.colorreq);
		onoffreq = (CheckBox)layout.findViewById(R.id.onoffreq);
		colorreq.setOnClickListener(this);
		onoffreq.setOnClickListener(this);
		barcontainer = (LinearLayout)layout.findViewById(R.id.light_seekbar_container);
		whitetext = (TextView)layout.findViewById(R.id.light_white_text);
		whitetext.setText("");
		whitebar =  (SeekBar)layout.findViewById(R.id.light_white_bar);
		whitebar.setMax(100);
		whitebar.setOnSeekBarChangeListener(this);
		
		brightext = (TextView) layout.findViewById(R.id.light_bright_text);
		brightext.setText("");
		brightbar = (SeekBar) layout.findViewById(R.id.light_blue_bar);
		brightbar.setMax(100);
		brightbar.setOnSeekBarChangeListener(this);
        
		
        mDialog =
            new AlertDialog.Builder(mContext)
                .setView(layout)
                .setNegativeButton(R.string.no,this)
                .setCancelable(true)
                .show();
        mDialog.setOnDismissListener(this);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setOnCancelListener(this);
        
        
        
    }
    
    @Override
	public void onColorChangeStart(View v, int color) {
		// TODO Auto-generated method stub
    	colordisplay.setBackgroundColor(color);
    	this.color = color;
	}

	@Override
	public void onColorChanged(View v, int color) {
		// TODO Auto-generated method stub
		colordisplay.setBackgroundColor(color);
		this.color = color;
	}

	@Override
	public void onColorChangeEnd(View v, int color) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub
		dialog.cancel();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v == confirm){
			if(savecolorlistener != null)
				savecolorlistener.onSaveColor(color, bright, light_onoff.isChecked());
			
		}
		else if(v == colorreq){
			if(colorreq.isChecked()){
				barcontainer.setVisibility(View.VISIBLE);
				colordisplay.setVisibility(View.VISIBLE);
				colorpick.setVisibility(View.VISIBLE);
				
			}else{
				barcontainer.setVisibility(View.GONE);
				colordisplay.setVisibility(View.GONE);
				colorpick.setVisibility(View.GONE);
			}
		}else if(v == onoffreq){
			if(onoffreq.isChecked()){
				light_onoff.setVisibility(View.VISIBLE);
			}else{
				light_onoff.setVisibility(View.GONE);
			}
		}
	}

	//对话窗口取消的过程中，移除所有任务
    /* public static interface DialogInterface.OnCancelListener 
     * 里面的抽象方法
     * The dialog that was canceled will be passed into themethod.
     * 继承了DialogInterface.OnCancelListener就要选择性实现他的抽象方法
     * */
    @Override
    public void onCancel(DialogInterface dialog)
    {
    		
    }
    
    //撤销该任务
    /* public static interface DialogInterface.OnDismissListener 
     * 里面的抽象方法
     *  The dialog that was dismissed will be passed into themethod.
     *  继承了DialogInterface.OnDismissListener就要选择性实现他的抽象方法
     * */
    @Override
    public void onDismiss(DialogInterface dialog)
    {
       
    }
    
   
    public interface OnSaveColorListener
    {
        void onSaveColor(int color, int bright, boolean ison);
    }
    
    
   //protected  abstract void setOnSavecolorListener(OnSaveColorListener listener);

  //接口可以实例化对象
    private OnSaveColorListener savecolorlistener;
    
    //覆写父类抽象方法
    public void SetOnSaveColorListener(OnSaveColorListener listener){
    	savecolorlistener = listener;
    }

	@Override
	public void setOnDissmissedListener(OnDissmissedListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		if(seekBar == brightbar)
		{
			bright = brightbar.getProgress();
		}
	}
    
   
}
