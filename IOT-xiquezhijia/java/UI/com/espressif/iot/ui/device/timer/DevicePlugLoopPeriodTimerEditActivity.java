package com.espressif.iot.ui.device.timer;

import com.espressif.iot.type.device.timer.EspDeviceLoopPeriodTimer;
import com.espressif.iot.type.device.timer.EspDeviceTimer;

public class DevicePlugLoopPeriodTimerEditActivity extends DeviceTimerEditLoopPeriodActivityAbs
{
    @Override
    protected String getEditAction()
    {
        return mActionValues[mActionSpinner.getSelectedItemPosition()];
    }
    
    @Override
    protected String getTimerAction(EspDeviceTimer  timer)
    {
        return ((EspDeviceLoopPeriodTimer)timer).getAction();
    }
}
