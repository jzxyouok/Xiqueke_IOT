package com.espressif.iot.ui.device.dialog;

import java.util.List;
import java.util.Vector;

import com.xiquezhijia.iot.R;
import com.espressif.iot.device.IEspDevice;
import com.espressif.iot.model.help.statemachine.EspHelpStateMachine;
import com.espressif.iot.type.device.IEspDeviceStatus;
import com.espressif.iot.ui.softap_sta_support.help.HelpSoftApStaSupportActivity;
import com.espressif.iot.user.IEspUser;
import com.espressif.iot.user.builder.BEspUser;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class DeviceDialogAbs implements EspDeviceDialogInterface, DialogInterface.OnDismissListener,
    DialogInterface.OnCancelListener
{
    protected IEspUser mUser;
    
    protected Context mContext;
    
    protected IEspDevice mDevice;
    
    protected AlertDialog mDialog;
    
    protected View mProgressContainer;
    
    private List<StatusTask> mTaskList;
    
    public DeviceDialogAbs(Context context, IEspDevice device)
    {
        mUser = BEspUser.getBuilder().getInstance();
        mContext = context;
        mDevice = device;
        
        mTaskList = new Vector<StatusTask>();
    }
    
    //覆写父类的方法
    /* 这是DialogInterface类的抽象方法
     * 供外部调用的，供实例化对象调用的
     * 
     * */
    @Override
    public void cancel()
    {
        if (mDialog != null)
        {
            mDialog.cancel();
        }
    }
    
    //覆写父类的方法
    //供外部调用的，供实例化对象调用的
    @Override
    public void dismiss()
    {
        if (mDialog != null)
        {
            mDialog.dismiss();
        }
    }

    //覆写父类的方法
    @Override
    public void show()
    {
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.device_dialog_abs, null);
        
        mProgressContainer = view.findViewById(R.id.progress_container);
        mProgressContainer.setVisibility(View.GONE);
        
        ViewGroup contentView = (ViewGroup)view.findViewById(R.id.device_dialog_content);
        //把具体的UI布局加载进来
        contentView.addView(getContentView(inflater));
        
        mDialog =
            new AlertDialog.Builder(mContext).setTitle(mDevice.getName())
                .setView(view)
                .setNegativeButton(R.string.esp_sss_device_dialog_exit, null)
                .show();
        mDialog.setOnDismissListener(this);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setOnCancelListener(this);
        
        
        new StatusTask().execute();
    }
    
    //对话窗口取消的过程中，移除所有任务
    /* public static interface DialogInterface.OnCancelListener 
     * 里面的抽象方法
     * The dialog that was canceled will be passed into themethod.
     * 继承了DialogInterface.OnCancelListener就要选择性实现他的抽象方法
     * */
    @Override
    public void onCancel(DialogInterface dialog)
    {
        for (StatusTask task : mTaskList)
        {
            task.cancel(true);
        }
        mTaskList.clear();
    }
    
    //撤销该任务
    /* public static interface DialogInterface.OnDismissListener 
     * 里面的抽象方法
     *  The dialog that was dismissed will be passed into themethod.
     *  继承了DialogInterface.OnDismissListener就要选择性实现他的抽象方法
     * */
    @Override
    public void onDismiss(DialogInterface dialog)
    {
        if (mOnDissmissedListener != null)
        {
            mOnDissmissedListener.onDissmissed(this);
        }
        
        EspHelpStateMachine helpMachine = EspHelpStateMachine.getInstance();
        if (helpMachine.isHelpModeUseSSSDevice())
        {
            helpMachine.transformState(true);
            ((HelpSoftApStaSupportActivity)mContext).onHelpUseSSSDevice();
        }
    }
    
    protected class StatusTask extends AsyncTask<IEspDeviceStatus, Void, Boolean>
    {
        private boolean mBroadcast;
        
        public StatusTask()
        {
            mBroadcast = false;
        }

        public StatusTask(boolean broadcast)
        {
            mBroadcast = broadcast;
        }
        
        @Override
        protected void onPreExecute()
        {
            mTaskList.add(this);
            mProgressContainer.setVisibility(View.VISIBLE);
        }
        
        @Override
        protected Boolean doInBackground(IEspDeviceStatus... params)
        {
            if (params.length > 0)
            {
                IEspDeviceStatus status = params[0];
                return mUser.doActionPostDeviceStatus(mDevice, status, mBroadcast);
            }
            else
            {
                return mUser.doActionGetDeviceStatus(mDevice);
            }
        }
        
        @Override
        protected void onPostExecute(Boolean result)
        {
            onExecuteEnd(result);
            
            mProgressContainer.setVisibility(View.GONE);
            
            mTaskList.remove(this);
        }
        
        @Override
        protected void onCancelled()
        {
        }
    }
    
    protected abstract void onExecuteEnd(boolean suc);
    
    protected abstract View getContentView(LayoutInflater inflater);
    
    //接口可以实例化对象
    private OnDissmissedListener mOnDissmissedListener;
    
    //覆写父类抽象方法
    public void setOnDissmissedListener(OnDissmissedListener listener)
    {
        mOnDissmissedListener = listener;
    }
}
