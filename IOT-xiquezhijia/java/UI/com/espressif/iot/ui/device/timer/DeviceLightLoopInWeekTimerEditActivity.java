package com.espressif.iot.ui.device.timer;



import android.graphics.Color;

import com.espressif.iot.type.device.timer.EspDeviceLoopWeekTimer;
import com.espressif.iot.type.device.timer.EspDeviceTimer;

public class DeviceLightLoopInWeekTimerEditActivity extends DeviceTimerEditLoopInWeekActivityAbs
{
    
    @Override
    protected String getEditAction()
    {
    	
    	String lightpara = "";
    	lightpara = "set:r" + "=" + Color.red(color) 
   			 + "g"  + "=" + Color.green(color)
   			 + "b"  + "=" + Color.blue(color)
   			 + "b"  + "=" + bright
   			 + "p"  + "=" + 1000
   			 + "#";
    	
    	return lightpara;
    }
    
    @Override
    protected String getTimerAction(EspDeviceTimer  timer)
    {
    	 
    	return ((EspDeviceLoopWeekTimer)timer).getTimeAction().get(0).getAction();
    }
    
}
