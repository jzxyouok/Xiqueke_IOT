package com.espressif.iot.ui.device.timer;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Color;

import com.espressif.iot.type.device.timer.EspDeviceLoopPeriodTimer;
import com.espressif.iot.type.device.timer.EspDeviceTimer;

public class DeviceLightLoopPeriodTimerEditActivity extends DeviceTimerEditLoopPeriodActivityAbs
{
    @Override
    protected String getEditAction()
    {
    	
    	String lightpara = "";
    	lightpara = "set:r" + "=" + Color.red(color) 
   			 + "g"  + "=" + Color.green(color)
   			 + "b"  + "=" + Color.blue(color)
   			 + "b"  + "=" + bright
   			 + "p"  + "=" + 1000
   			 + "#";
    	
    	return lightpara;
    }
    
    @Override
    protected String getTimerAction(EspDeviceTimer  timer)
    {
        return ((EspDeviceLoopPeriodTimer)timer).getAction();
    }
}
