package com.espressif.iot.ui.view;

import com.xiquezhijia.iot.R;

import android.app.AlertDialog;
import android.content.Context;

public class NoBgDialog extends AlertDialog
{
    public NoBgDialog(Context context)
    {
        super(context, R.style.TransparentBgDialog);
    }
}
