package com.espressif.iot.ui.main;


import com.xiquezhijia.iot.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class XqHomepage extends Activity implements OnClickListener{

	private ImageView zonectrl;
	private ImageView scencectrl;
	private ImageView voicectrl;
	private ImageView safectrl;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainpage);
		findView();
		initView();
	}

	private void initView() {
		// TODO Auto-generated method stub
		
	}

	private void findView() {
		// TODO Auto-generated method stub
		zonectrl = (ImageView)findViewById(R.id.zonectrl);
		scencectrl = (ImageView)findViewById(R.id.scenetrl);
		voicectrl = (ImageView)findViewById(R.id.sppechctrl);
		safectrl = (ImageView)findViewById(R.id.safetrl);
		
		zonectrl.setOnClickListener(this);
		scencectrl.setOnClickListener(this);
		voicectrl.setOnClickListener(this);
		safectrl.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v == zonectrl){
			Intent mintent = new Intent(this, XqZoneActivty.class);
			startActivity(mintent);
		}else if(v == scencectrl){
			
		}else if(v == voicectrl){
			Intent mintent = new Intent(this, XqSpeechActivity.class);
			startActivity(mintent);
		}else if(v == safectrl){
			
		}
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	

}
