package com.espressif.iot.ui.main;

import java.util.ArrayList;
import java.util.List;

import com.espressif.iot.db.IOTXqzoneDBManager;
import com.espressif.iot.db.greenrobot.daos.XqzoneDB;
import com.espressif.iot.ui.main.XqZoneActivty.ZoneAdapter;


import com.espressif.iot.ui.main.XqZoneActivty.ZoneAdapter.ZoneHolder;
import com.espressif.iot.util.EspStrings.Key;
import com.xiquezhijia.iot.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AddZoneActivity extends Activity implements OnClickListener, OnItemClickListener{
	
	private Button back;
	private Button save;
	private EditText roomname;
	private GridView zonegridview;
	private List<XqzoneDB> zonelist = new ArrayList<XqzoneDB>();
	private ZoneAdapter  mzoneadapter;
	private int[] resid={R.drawable.sofa, R.drawable.bedroom, R.drawable.diningtable, R.drawable.schoolroom, R.drawable.officetable,
						 R.drawable.childrenbed, R.drawable.cookhouse, R.drawable.looroom, R.drawable.gazebo};
	private int iconresid;
	private boolean[] iconlistcheck=new boolean[resid.length];
	IOTXqzoneDBManager qzoneDBManage;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.xqaddzone_activity);
		back = (Button)findViewById(R.id.left_icon);
		save = (Button)findViewById(R.id.right_icon);
		roomname = (EditText)findViewById(R.id.roomnameedit);
		back.setOnClickListener(this);
		save.setOnClickListener(this);
		zonegridview = (GridView)findViewById(R.id.zone);
		mzoneadapter = new ZoneAdapter(this);
		zonegridview.setAdapter(mzoneadapter);
		zonegridview.setFocusable(true);
		zonegridview.setOnItemClickListener(this);
		
		
		qzoneDBManage = IOTXqzoneDBManager.getInstance();
		zonelist = qzoneDBManage.getAllXqzoneDBList();
		iconresid = resid[0];
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		String txt = null;
		if(v == back){
			finish();
		}else if(v == save){
			txt =roomname.getText().toString();
			if(TextUtils.isEmpty(txt)){
				Toast.makeText(getBaseContext(), getString(R.string.pleaseinputroomname), Toast.LENGTH_SHORT).show();
				return;
			}
			//create new zone
			//check the list hasn't the same name
			if(zonelist.size()>0)
			for(XqzoneDB mxqzonedb: zonelist){
				if(mxqzonedb.getXqzoneName().equalsIgnoreCase(roomname.getText().toString())){
					Toast.makeText(getBaseContext(), getString(R.string.pleaserename), Toast.LENGTH_SHORT).show();
					return;
				}
			}
			boolean iconchecked=false;
			for(int i=0; i<iconlistcheck.length; i++){
				if(iconlistcheck[i] == true)
				{
					iconchecked = true;
					break;
				}
			}
			if(iconchecked == false){
				Toast.makeText(getBaseContext(), getString(R.string.slestroomicon), Toast.LENGTH_SHORT).show();
				return;	
			}
			qzoneDBManage.insertOrReplace(iconresid, txt);
			Intent mfintent = new Intent();
			mfintent.putExtra(Key.XQZONENAME_KEY, txt);
			setResult(RESULT_OK, mfintent);
			finish();
		}
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int pos, long arg3) {
		// TODO Auto-generated method stub
		Log.e("onItemClick", "getpos"  + pos);
		for(int i=0; i<iconlistcheck.length; i++){
			if(pos == i)
			{
				iconlistcheck[i]=!iconlistcheck[i];
			}
			else
				iconlistcheck[i]=false;
		}
		
		iconresid = resid[pos];
		mzoneadapter.notifyDataSetChanged();
	}
	class ZoneAdapter extends BaseAdapter {

		protected static final String TAG = "ZoneAdapter";
		private AddZoneActivity mContext;
		//private List<XqzoneDB> mzoneList = new ArrayList<XqzoneDB>();
		private LayoutInflater mInflater;
		private float x, ux;
		private byte[] mContent;  
		private Bitmap myBitmap;
		
		//private boolean isDelete = false;
		
		public ZoneAdapter(Context context){
			mContext = (AddZoneActivity)context;
			
			mInflater = LayoutInflater.from(context);
		}
		
		
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			//Log.d(TAG, "get size is: " + mSwitchList.size());
			return resid.length;
		}

		@Override
		public Object getItem(int pos) {
			// TODO Auto-generated method stub
			if(pos < 0 || pos >= resid.length)
			{
				return null;
			}
			else{
				return resid[pos];
			}
		}

		@Override
		public long getItemId(int pos) {
			// TODO Auto-generated method stub
			Log.d(TAG, "get item id");
			return pos;
		}
		

		@Override
		public View getView(int pos, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ZoneHolder viewHolder = null;
			Log.e(TAG, "getView" + "pos=" +pos);
			
			//*
			if(convertView == null){
				viewHolder = new ZoneHolder();
				Log.d(TAG, "create convertview");
				convertView = mInflater.inflate(R.layout.xqaddzone_unit, null);
				
				{
					viewHolder.zoneicon = (View)convertView.findViewById(R.id.root);
					viewHolder.checkbox = (CheckBox)convertView.findViewById(R.id.iconcheck);
					viewHolder.zoneicon.setBackgroundResource(resid[pos]);
					viewHolder.checkbox.setClickable(false);
					
				}
				viewHolder.pos = pos;
				convertView.setTag(viewHolder);
				convertView.setBackgroundResource(resid[pos]);
				
			}
			else{
				
				viewHolder = (ZoneHolder) convertView.getTag();
				if(pos < resid.length){
					convertView.setBackgroundResource(resid[pos]);
				}
				
				
			}
			
			if(iconlistcheck[pos]){
				viewHolder.checkbox.setChecked(true);
			}else
				viewHolder.checkbox.setChecked(false);
		
			return convertView;
		}


		class ZoneHolder{
		
			View zoneicon;
			CheckBox checkbox;
			int pos = 0;
			ZoneHolder(){
				
			}
		}
	}

}
