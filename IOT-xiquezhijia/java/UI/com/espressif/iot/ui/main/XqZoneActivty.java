package com.espressif.iot.ui.main;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;









import com.espressif.iot.db.IOTXqzoneDBManager;
import com.espressif.iot.db.greenrobot.daos.XqzoneDB;
import com.espressif.iot.object.db.IXqzoneDBManager;
import com.espressif.iot.util.EspStrings.Key;
import com.xiquezhijia.iot.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class XqZoneActivty extends Activity implements OnClickListener, OnItemClickListener{

	private Button back;
	private Button addzone;
	private GridView zonegridview;
	private List<XqzoneDB> zonelist = new ArrayList<XqzoneDB>();
	private ZoneAdapter  mzoneadapter;
	private static IOTXqzoneDBManager XqzoneDBManagerinstance = null;
	private XqzoneDB curzone;
	private static final int REQ_ADDZONE= 10000;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.xqzone_activity);
		back = (Button)findViewById(R.id.left_icon);
		addzone = (Button)findViewById(R.id.right_icon);
		zonegridview = (GridView)findViewById(R.id.zone);
		back.setOnClickListener(this);
		addzone.setOnClickListener(this);
		XqzoneDBManagerinstance =IOTXqzoneDBManager.getInstance();
		initZoneList();
		//ShowCurrentZone();
		mzoneadapter = new ZoneAdapter(this, zonelist);
		zonegridview.setAdapter(mzoneadapter);
		zonegridview.setOnItemClickListener(this);
	}
	private void initZoneList(){
		zonelist.addAll(XqzoneDBManagerinstance.getAllXqzoneDBList());
		if(zonelist.size()>0)
			curzone = zonelist.get(0);
		
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	
	class ZoneAdapter extends BaseAdapter {

		protected static final String TAG = "ZoneAdapter";
		private XqZoneActivty mContext;
		private List<XqzoneDB> mzoneList = new ArrayList<XqzoneDB>();
		private LayoutInflater mInflater;
		private float x, ux;
		private byte[] mContent;  
		private Bitmap myBitmap;
		
		//private boolean isDelete = false;
		
		public ZoneAdapter(Context context, List<XqzoneDB> list){
			mContext = (XqZoneActivty)context;
			mzoneList = list;
			mInflater = LayoutInflater.from(context);
		}
		
		public void SetList(List<XqzoneDB> list){
			mzoneList = list;

		}
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			//Log.d(TAG, "get size is: " + mSwitchList.size());
			return mzoneList.size();
		}

		@Override
		public Object getItem(int pos) {
			// TODO Auto-generated method stub
			if(pos < 0 || pos >= mzoneList.size())
			{
				return null;
			}
			else{
				return mzoneList.get(pos);
			}
		}

		@Override
		public long getItemId(int pos) {
			// TODO Auto-generated method stub
			Log.d(TAG, "get item id");
			return pos;
		}
		

		@Override
		public View getView(int pos, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ZoneHolder viewHolder = null;
			Log.e(TAG, "getView");
			
			//*
			if(convertView == null){
				viewHolder = new ZoneHolder();
				Log.d(TAG, "create convertview");
				convertView = mInflater.inflate(R.layout.xqzone_unit, null);
				
				
				
				{
					viewHolder.zoneicon = (ImageView)convertView.findViewById(R.id.zoneicon);
					viewHolder.zonename = (TextView) convertView.findViewById(R.id.zonename);
					viewHolder.childcount = (TextView) convertView.findViewById(R.id.childcount);
					
				}
				viewHolder.pos = pos;
				convertView.setTag(viewHolder);
				
			}
			else{
				
				viewHolder = (ZoneHolder) convertView.getTag();
				
			}
			// 设置高度和宽度    
			Point point = new Point();
	        getWindowManager().getDefaultDisplay().getSize(point);
			          
			convertView.setLayoutParams(new GridView.LayoutParams(point.x/2, point.x/2));
						
			
			//GoneDelete(holder);
			XqzoneDB mzone = mzoneList.get(pos);
			if(mzone != null )
			{
				viewHolder.zoneicon.setImageResource(mzone.getIconid());
				viewHolder.zonename.setText(mzone.getXqzoneName());
				viewHolder.childcount.setText("没有设备");
				viewHolder.zonename.setVisibility(View.VISIBLE);
			}
			else{
				viewHolder.zoneicon.setVisibility(View.GONE);
				viewHolder.zonename.setVisibility(View.GONE);
				viewHolder.childcount.setVisibility(View.GONE);
			}
		
			return convertView;
		}


		class ZoneHolder{
		
			ImageView zoneicon;
			TextView zonename;
			
			TextView childcount;
			int pos = 0;
			ZoneHolder(){
				
			}
		}
	}


	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if(view == back){
			finish();
		}else if(view == addzone){
			Intent minten = new Intent(this, AddZoneActivity.class);
			startActivityForResult(minten, REQ_ADDZONE);
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(REQ_ADDZONE == requestCode && RESULT_OK==resultCode ){
			String zonename = data.getStringExtra(Key.XQZONENAME_KEY);
			if(TextUtils.isEmpty(zonename)){
				Toast.makeText(getBaseContext(), getString(R.string.pleaseinputroomname), Toast.LENGTH_SHORT).show();
				return;
			}
			/*	show the zone just added successfully 
			 * */
			initZoneList();
			mzoneadapter.notifyDataSetChanged();
		}
	}
	private void ShowCurrentZone() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int pos, long arg3) {
		// TODO Auto-generated method stub
		Log.e("zone onItemClick", "区域" + zonelist.get(pos).getXqzoneName() + "需要添加或者控制设备");
		Intent mintent = new Intent(this, XqZoneDetailActivty.class);
		mintent.putExtra(Key.XQZONENAME_KEY, zonelist.get(pos).getXqzoneName());
		startActivity(mintent);
	}

}
