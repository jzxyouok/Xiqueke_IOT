package com.espressif.iot.ui.settings;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.xiquezhijia.iot.R;
import com.espressif.iot.base.api.EspBaseApiUtil;
import com.espressif.iot.base.application.EspApplication;
import com.espressif.iot.base.net.rest.EspHttpDownloadUtil.ProgressUpdateListener;
import com.espressif.iot.log.LogConfigurator;
import com.espressif.iot.log.ReadLogTask;
import com.espressif.iot.type.upgrade.EspUpgradeApkResult;
import com.espressif.iot.type.user.EspLoginResult;
import com.espressif.iot.ui.configure.DeviceConfigureActivity;
import com.espressif.iot.ui.main.LoginTask;
import com.espressif.iot.ui.main.RegisterActivity;
import com.espressif.iot.ui.main.LoginThirdPartyDialog;
import com.espressif.iot.ui.main.LoginThirdPartyDialog.OnLoginListener;
import com.espressif.iot.user.IEspUser;
import com.espressif.iot.user.builder.BEspUser;
import com.espressif.iot.util.EspStrings;
import com.lishate.update.VersionItem;
import com.lishate.utility.GobalDef;
import com.lishate.utility.Utility;

public class SettingsFragment extends PreferenceFragment implements OnPreferenceChangeListener
{
    private final Logger log = Logger.getLogger(getClass());
    
    private static final String KEY_ACCOUNT = "account";
    private static final String KEY_ACCOUNT_REGISTER = "account_register";
    private static final String KEY_ACCOUNT_AUTO_LOGIN = "account_auto_login";
    private static final String KEY_AUTO_REFRESH_DEVICE = "device_auto_refresh";
    private static final String KEY_AUTO_CONFIGURE_DEVICE = "device_auto_configure";
    private static final String KEY_VERSION_NAME = "version_name";
    private static final String KEY_VERSION_UPGRADE = "version_upgrade";
    private static final String KEY_VERSION_LOG = "version_log";
    private static final String KEY_STORE_LOG = "store_log";
    private static final String KEY_READ_LOG = "read_log";
    private static final String KEY_CLEAR_LOG = "clear_log";
    //	add by qin 20150811
	private final static int MsgNoVersionID = 1;
	private final static int MsgUpdateVersionID = 2;
	private final static int MsgDownloadFail = 3;
	private final static int MsgNoNet = 4;
	
	private VersionItem getVI = null;
	
	
    private static final String DEFAULT_VERSION_LOG_URL = "file:///android_asset/html/en_us/update.html";
    /**
     * The url for WebView
     */
    private static final String VERSION_LOG_URL = "file:///android_asset/html/%locale/update.html";
    /**
     * The path for AssetManager
     */
    private static final String VERSION_LOG_PATH = "html/%locale/update.html";
    
    private static final int REQUEST_REGISTER = 1000;
    
    private Preference mAccountPre;
    private Preference mAccountRegisterPre;
    private CheckBoxPreference mAutoLoginPre;
    private ListPreference mAutoRefreshDevicePre;
    private ListPreference mAutoConfigureDevicePre;
    private Preference mVersionNamePre;
    private Preference mVersionUpgradePre;
    private Preference mVersionLogPre;
    private CheckBoxPreference mStoreLogPre;
    private Preference mReadLogPre;
    private Preference mClearLogPre;
    
    private IEspUser mUser;
    
    private UpgradeApkTask mUpgradeApkTask;
    
    private AlertDialog mLogDialog;
    private List<String> mLogList;
    private ArrayAdapter<String> mLogAdapter;
    
    private SharedPreferences mShared;
    
    private LocalBroadcastManager mBroadcastManager;
    
    private LoginThirdPartyDialog mThirdPartyLoginDialog;
    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        addPreferencesFromResource(R.xml.settings);
        
        mUser = BEspUser.getBuilder().getInstance();
        mShared = getActivity().getSharedPreferences(EspStrings.Key.SETTINGS_NAME, Context.MODE_PRIVATE);
        mBroadcastManager = LocalBroadcastManager.getInstance(getActivity());
        
        // About Account
        mAccountPre = findPreference(KEY_ACCOUNT);
        mAccountRegisterPre = findPreference(KEY_ACCOUNT_REGISTER);
        if (mUser.isLogin())
        {
            String userEmail = mUser.getUserEmail();
            mAccountPre.setTitle(userEmail);
            
            getPreferenceScreen().removePreference(mAccountRegisterPre);
        }
        else
        {
            mAccountPre.setTitle(R.string.esp_settings_account_not_login);
            mAccountPre.setSummary(R.string.esp_settings_account_not_login_summary);
        }
        
        mThirdPartyLoginDialog = new LoginThirdPartyDialog(getActivity());
        mThirdPartyLoginDialog.setOnLoginListener(mThirdPartyLoginListener);
        
        mAutoLoginPre = (CheckBoxPreference)findPreference(KEY_ACCOUNT_AUTO_LOGIN);
        mAutoLoginPre.setChecked(mUser.isAutoLogin());
        mAutoLoginPre.setOnPreferenceChangeListener(this);
        mAutoLoginPre.setEnabled(mUser.isLogin());
        
        // About Device
        mAutoRefreshDevicePre = (ListPreference)findPreference(KEY_AUTO_REFRESH_DEVICE);
        String autoRefreshTime = "" + mShared.getLong(EspStrings.Key.SETTINGS_KEY_DEVICE_AUTO_REFRESH, 0);
        mAutoRefreshDevicePre.setValue(autoRefreshTime);
        mAutoRefreshDevicePre.setSummary(mAutoRefreshDevicePre.getEntry());
        mAutoRefreshDevicePre.setOnPreferenceChangeListener(this);
        
        mAutoConfigureDevicePre = (ListPreference)findPreference(KEY_AUTO_CONFIGURE_DEVICE);
        int defaultAutoConfigureValue = DeviceConfigureActivity.DEFAULT_AUTO_CONFIGRUE_VALUE;
        String autoConfigureValue =
            "" + mShared.getInt(EspStrings.Key.SETTINGS_KEY_DEVICE_AUTO_CONFIGURE, defaultAutoConfigureValue);
        mAutoConfigureDevicePre.setValue(autoConfigureValue);
        mAutoConfigureDevicePre.setSummary(mAutoConfigureDevicePre.getEntry());
        mAutoConfigureDevicePre.setOnPreferenceChangeListener(this);
        
        // About Version
        mVersionNamePre = findPreference(KEY_VERSION_NAME);
        String versionName = EspApplication.sharedInstance().getVersionName();
        mVersionNamePre.setSummary(versionName);
        
        mVersionUpgradePre = findPreference(KEY_VERSION_UPGRADE);
        if (mVersionUpgradePre != null && EspApplication.GOOGLE_PALY_VERSION)
        {
            getPreferenceScreen().removePreference(mVersionUpgradePre);
        }
        
        mVersionLogPre = findPreference(KEY_VERSION_LOG);
        
        // About DEBUG
        mStoreLogPre = (CheckBoxPreference)findPreference(KEY_STORE_LOG);
        mStoreLogPre.setOnPreferenceChangeListener(this);
        mReadLogPre = findPreference(KEY_READ_LOG);
        boolean store = mShared.getBoolean(EspStrings.Key.SETTINGS_KEY_STORE_LOG, false);
        mStoreLogPre.setChecked(store);
        mClearLogPre = findPreference(KEY_CLEAR_LOG);
        
        mLogList = new ArrayList<String>();
        mLogAdapter =
            new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, mLogList);
        mLogDialog = new AlertDialog.Builder(getActivity()).setAdapter(mLogAdapter, null).create();
    }
    
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        
        if (mUpgradeApkTask != null)
        {
            mUpgradeApkTask.cancel(true);
        }
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_REGISTER)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                String email = data.getStringExtra(EspStrings.Key.REGISTER_NAME_EMAIL);
                String password = data.getStringExtra(EspStrings.Key.REGISTER_NAME_PASSWORD);
                
                login(email, password);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    
    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference)
    {
        if (preference == mVersionUpgradePre)
        {
            updateApk();
            return true;
        }
        else if (preference == mVersionLogPre)
        {
            showUpdateLogDialog();
            return true;
        }
        else if (preference == mReadLogPre)
        {
            readDebugLog();
            return true;
        }
        else if (preference == mClearLogPre)
        {
            clearDebugLog();
            return true;
        }
        else if (preference == mAccountPre)
        {
            if (!mUser.isLogin())
            {
                showLoginDialog();
                return true;
            }
        }
        else if (preference == mAccountRegisterPre)
        {
            Intent i = new Intent(getActivity(), RegisterActivity.class);
            startActivityForResult(i, REQUEST_REGISTER);
            return true;
        }
        
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }
    
    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue)
    {
        if (preference == mAutoLoginPre)
        {
            boolean autoLogin = (Boolean)newValue;
            mUser.saveUserInfoInDB(false, autoLogin);
            return true;
        }
        else if (preference == mAutoRefreshDevicePre)
        {
            String time = newValue.toString();
            mAutoRefreshDevicePre.setValue(time);
            mAutoRefreshDevicePre.setSummary(mAutoRefreshDevicePre.getEntry());
            mShared.edit().putLong(EspStrings.Key.SETTINGS_KEY_DEVICE_AUTO_REFRESH, Long.parseLong(time)).commit();
            return true;
        }
        else if (preference == mAutoConfigureDevicePre)
        {
            String value = newValue.toString();
            mAutoConfigureDevicePre.setValue(value);
            mAutoConfigureDevicePre.setSummary(mAutoConfigureDevicePre.getEntry());
            mShared.edit().putInt(EspStrings.Key.SETTINGS_KEY_DEVICE_AUTO_CONFIGURE, Integer.parseInt(value)).commit();
            return true;
        }
        else if (preference == mStoreLogPre)
        {
            onStoreDebugLogChanged((Boolean) newValue);
            return true;
        }
        
        return false;
    }
    
    //******** About Account *********//
    private void showLoginDialog()
    {
        View view = getActivity().getLayoutInflater().inflate(R.layout.login_dialog, null);
        final EditText emailEdT = (EditText)view.findViewById(R.id.login_edt_account);
        final EditText pwdEdt = (EditText)view.findViewById(R.id.login_edt_password);
        final TextView thirdPartyLoginTV = (TextView)view.findViewById(R.id.login_text_third_party);
        
        final AlertDialog dialog = new AlertDialog.Builder(getActivity()).setTitle(R.string.esp_login_login)
            .setView(view)
            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
            {
                
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    String email = emailEdT.getText().toString();
                    String password = pwdEdt.getText().toString();
                    if (!TextUtils.isEmpty(email))
                    {
                        login(email, password);
                    }
                }
            })
            .show();
        
        thirdPartyLoginTV.setOnClickListener(new View.OnClickListener()
        {
            
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                mThirdPartyLoginDialog.show();
            }
        });
    }
    
    private OnLoginListener mThirdPartyLoginListener = new OnLoginListener()
    {
        
        @Override
        public void onLoginComplete(EspLoginResult result)
        {
            if (result == EspLoginResult.SUC)
            {
                loginSuc();
            }
        }
    };
    
    private void login(final String email, final String password)
    {
        new LoginTask(getActivity(), email, password, mAutoLoginPre.isChecked())
        {
            public void loginResult(EspLoginResult result)
            {
                if (result == EspLoginResult.SUC)
                {
                    loginSuc();
                }
            }
        }.execute();
    }
    
    private void loginSuc()
    {
        mAccountPre.setTitle(mUser.getUserEmail());
        mAccountPre.setSummary("");
        mAutoLoginPre.setEnabled(true);
        
        Preference registerPre = findPreference(KEY_ACCOUNT_REGISTER);
        if (registerPre != null)
        {
            getPreferenceScreen().removePreference(registerPre);
        }
        
        mBroadcastManager.sendBroadcast(new Intent(EspStrings.Action.LOGIN_NEW_ACCOUNT));
    }
    
    //******** About Version *********//
    private void updateApk()
    {
        ConnectivityManager cm = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_MOBILE)
        {
            // Show dialog to hint using mobile data now  3G 或者GPRS
            new AlertDialog.Builder(getActivity()).setTitle(R.string.esp_upgrade_apk_mobile_data_title)
                .setMessage(R.string.esp_upgrade_apk_mobile_data_msg)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        //executeUpgradeApkTask();
                    }
                })
                .show();
        }
        else
        {
            //executeUpgradeApkTask();
        }
    }
    
    private void executeUpgradeApkTask()
    {
//        mUpgradeApkTask = new UpgradeApkTask();
//        mUpgradeApkTask.execute();
        
		new Thread(new CheckVersionTask()).start();
    }
    
    private class CheckVersionTask implements Runnable{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			Message msg = Message.obtain();
			if(Utility.CheckNetwork(getActivity()) == false){
				msg.what = MsgNoNet;
				taskHandler.sendMessage(msg);
			}
			else{
				if(com.lishate.update.Utility.CheckServerVersionUpdate()){
					VersionItem vi = com.lishate.update.Utility.GetVersionItem();
					if(vi == null){
						msg.what = MsgNoVersionID;
						taskHandler.sendMessage(msg);
					}
					else{
						getVI = vi;
						msg.what = MsgUpdateVersionID;
						taskHandler.sendMessage(msg);
					}
				}
				else{
					msg.what = MsgNoVersionID;
					taskHandler.sendMessage(msg);
				}
			}
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
    private Handler taskHandler = new Handler(){

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			switch(msg.what){
			case MsgNoVersionID:
				Toast.makeText(getActivity(), "" + getString(R.string.esp_upgrade_apk_status_not_found), Toast.LENGTH_SHORT).show();
				break;
			case MsgNoNet:
				Toast.makeText(getActivity(), "" + getString(R.string.esp_login_result_network_unaccessible), Toast.LENGTH_SHORT).show();
				break;
			case MsgDownloadFail:
				AlertDialog tmpdialog = new AlertDialog.Builder(getActivity()).setTitle(R.string.app_name)
						.setMessage(R.string.esp_upgrade_apk_status_download_failed)
						.setCancelable(false)
						.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener(){

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								//tmpdialog.dismiss();
								//StartActivity();
							}
							
						}).create();
				tmpdialog.show();
				break;
			case MsgUpdateVersionID:
				Dialog dialog = new Dialog(getActivity(), R.style.exitdialog);
				dialog.setContentView(R.layout.exit_dialog);
				dialog.show();
				
				Button yes = (Button)dialog.findViewById(R.id.exitdialog_yes);
				Button no = (Button)dialog.findViewById(R.id.exitdialog_no);
				TextView text = (TextView)dialog.findViewById(R.id.exitdialog_text);
				text.setText(R.string.update_new);
				yes.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						DownloadTask dt = new DownloadTask();
						dt.execute(new Void[0]);
					}
				});
				no.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

				break;
			}
		}
		
	};
    private  class DownloadTask extends AsyncTask<Void, Integer, File>{

		ProgressDialog progressDialog;
		
		@Override
		protected File doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			File f = null;
			try{
				VersionItem vi = getVI;
				String url = vi.getUrl();//"http://115.28.45.50/update/2/switch.apk";
				//int length = Utility.GetHttpLength(url);
				HttpURLConnection conn = (HttpURLConnection)new URL(url).openConnection();
				conn.setConnectTimeout(5000);
				InputStream is = conn.getInputStream();
				int length = conn.getContentLength();
				progressDialog.setMax(length);
				f = new File(GobalDef.Instance.getUpdatePath(), "update.apk");
				FileOutputStream os = new FileOutputStream(f);
				try{
					
					byte[] buf = new byte[1024];
					int i = 0;
					while(true){
						int j = is.read(buf);
						if(j <= 0){
							if(i >= length){
								return f;
							}
							else{
								return null;
							}
							
						}
						os.write(buf,0,j);
						i+=j;
						Integer[] temps = new Integer[1];
						temps[0] = Integer.valueOf(i);
						//把进度信息广播出去  onProgressUpdate会被回调
						publishProgress(temps);
					}
				}
				catch(Throwable t){
					t.printStackTrace();
				}
				finally{
					is.close();
					os.close();
					
				}
			}
			catch(Throwable t)
			{
				t.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(File result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			progressDialog.dismiss();
			if(result != null){
				Intent installIntent = new Intent(Intent.ACTION_VIEW);
				installIntent.setDataAndType(Uri.fromFile(result), "application/vnd.android.package-archive");
				//installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(installIntent);
				//LoadActivity.this.finish();
				//System.exit(0);
			}
			else{
				Message msg = Message.obtain();
				msg.what = MsgDownloadFail;
				taskHandler.sendMessage(msg);
			}
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progressDialog.setCanceledOnTouchOutside(false);
			progressDialog.setMessage(getString(R.string.apkdownload));
			progressDialog.show();
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			progressDialog.setProgress(values[0].intValue());
			super.onProgressUpdate(values);
		}
		
		
		
	}
    private class UpgradeApkTask extends AsyncTask<Void, Integer, EspUpgradeApkResult>
    {
        @Override
        protected void onPreExecute()
        {
            mVersionUpgradePre.setEnabled(false);
        }
        
        @Override
        protected EspUpgradeApkResult doInBackground(Void... arg0)
        {
            return EspBaseApiUtil.upgradeApk(mUpdateListener);
        }
        
        @Override
        protected void onPostExecute(EspUpgradeApkResult result)
        {
            mVersionUpgradePre.setEnabled(true);
            
            switch (result)
            {
                case UPGRADE_COMPLETE:
                    Toast.makeText(getActivity(), R.string.esp_upgrade_apk_status_complete_toast, Toast.LENGTH_LONG)
                        .show();
                    mVersionUpgradePre.setSummary(R.string.esp_upgrade_apk_status_complete);
                    break;
                case DOWNLOAD_FAILED:
                    mVersionUpgradePre.setSummary(R.string.esp_upgrade_apk_status_download_failed);
                    break;
                case LOWER_VERSION:
                    mVersionUpgradePre.setSummary(R.string.esp_upgrade_apk_status_lower_version);
                    break;
                case NOT_FOUND:
                    mVersionUpgradePre.setSummary(R.string.esp_upgrade_apk_status_not_found);
                    break;
            }
        }
        
        @Override
        protected void onProgressUpdate(Integer... values)
        {
            int percent = values[0];
            mVersionUpgradePre.setSummary(getString(R.string.esp_upgrade_apk_downloading, percent));
        }
        
        /**
         * Update download progress
         */
        private ProgressUpdateListener mUpdateListener = new ProgressUpdateListener()
        {
            
            @Override
            public void onProgress(long downloadSize, double percent)
            {
                int per = (int)(percent * 100);
                publishProgress(per);
            }
            
        };
    }
    
    /**
     * Show update log
     */
    private void showUpdateLogDialog()
    {
        /*
         * check for the full language + country resource, if not there, check for the only language resource, if not
         * there again, use default(en_us) language log
         */
        boolean isLogFileExist;
        Locale locale = Locale.getDefault();
        String languageCode = locale.getLanguage().toLowerCase(Locale.US);
        String countryCode = locale.getCountry().toLowerCase(Locale.US);
        
        String folderName = languageCode + "_" + countryCode;
        String path = VERSION_LOG_PATH.replace("%locale", folderName);
        // check full language + country resource
        isLogFileExist = isAssetFileExist(path);
        
        if (!isLogFileExist)
        {
            folderName = languageCode;
            path = VERSION_LOG_PATH.replace("%locale", folderName);
            
            // check the only language resource
            isLogFileExist = isAssetFileExist(path);
        }
        
        String url;
        if (isLogFileExist)
        {
            url = VERSION_LOG_URL.replaceAll("%locale", folderName);
        }
        else
        {
            url = DEFAULT_VERSION_LOG_URL;
        }
        
        WebView webview = new WebView(getActivity());
        webview.loadUrl(url);
        
        new AlertDialog.Builder(getActivity()).setView(webview).show();
    }
    
    /**
     * Check whether the log file exist
     * @param path
     * @return
     */
    private boolean isAssetFileExist(String path)
    {
        boolean result = true;
        
        final AssetManager am = getActivity().getAssets();
        InputStream is = null;
        try
        {
            is = am.open(path);
        }
        catch (Exception ignored)
        {
            result = false;
        }
        finally
        {
            if (is != null)
            {
                try
                {
                    is.close();
                }
                catch (Exception ignored)
                {
                }
            }
        }
        
        return result;
    }
    
    //******** About Debug *********//
    private void readDebugLog()
    {
        final ReadLogTask task = new ReadLogTask()
        {
            @Override
            protected void onPreExecute()
            {
                mLogList.clear();
                mLogAdapter.notifyDataSetChanged();
                mLogDialog.show();
            }
            
            @Override
            protected void onProgressUpdate(String... values)
            {
                mLogList.add("\n" + values[0] + "\n");
                mLogAdapter.notifyDataSetChanged();
                mLogDialog.getListView().setSelection(mLogList.size() - 1);
            }
            
            @Override
            protected void onPostExecute(Boolean result)
            {
                if (mLogList.isEmpty())
                {
                    mLogList.add(getActivity().getString(R.string.esp_settings_debug_read_log_no_log));
                    mLogAdapter.notifyDataSetChanged();
                }
            }
        };
        mLogDialog.setOnDismissListener(new DialogInterface.OnDismissListener()
        {
            
            @Override
            public void onDismiss(DialogInterface dialog)
            {
                task.cancel(true);
            }
        });
        task.execute();
    }
    
    private void onStoreDebugLogChanged(Boolean store)
    {
        mShared.edit().putBoolean(EspStrings.Key.SETTINGS_KEY_STORE_LOG, store).commit();
        Logger root = Logger.getRootLogger();
        if (store)
        {
            root.addAppender(LogConfigurator.createFileAppender());
            log.debug("Open log file store");
        }
        else
        {
            root.removeAppender(LogConfigurator.APPENDER_NAME);
        }
    }
    
    private void clearDebugLog()
    {
        File file = new File(LogConfigurator.DefaultLogFileDirPath);
        if (file.isDirectory())
        {
            File[] logFiles = file.listFiles();
            for (int i = 0; i < logFiles.length; i++)
            {
                logFiles[i].delete();
            }
        }
        else
        {
            log.warn("Delete path is not directry");
        }
    }
    
}
