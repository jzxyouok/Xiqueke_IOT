package com.espressif.iot.action.device.common;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import com.espressif.iot.base.api.EspBaseApiUtil;
import com.espressif.iot.base.net.rest.mesh.EspSocketClient;
import com.espressif.iot.command.device.common.EspCommandDeviceDiscoverLocal;
import com.espressif.iot.command.device.common.EspCommandDeviceSynchronizeInternet;
import com.espressif.iot.command.device.common.IEspCommandDeviceDiscoverLocal;
import com.espressif.iot.command.device.common.IEspCommandDeviceSynchronizeInternet;
import com.espressif.iot.device.IEspDevice;
import com.espressif.iot.device.cache.IEspDeviceCache;
import com.espressif.iot.device.cache.IEspDeviceCache.NotifyType;
import com.espressif.iot.model.device.EspDevice;
import com.espressif.iot.model.device.cache.EspDeviceCache;
import com.espressif.iot.type.device.IEspDeviceState;
import com.espressif.iot.type.net.IOTAddress;

public class EspActionDeviceSynchronizeInterentDiscoverLocal implements
    IEspActionDeviceSynchronizeInterentDiscoverLocal
{
    private final static Logger log = Logger.getLogger(EspActionDeviceSynchronizeInterentDiscoverLocal.class);
    
    private List<IOTAddress> doCommandDeviceDiscoverLocal()
    {
        IEspCommandDeviceDiscoverLocal command = new EspCommandDeviceDiscoverLocal();
        return command.doCommandDeviceDiscoverLocal();
    }
    
    private List<IEspDevice> doCommandDeviceSynchronizeInternet(String userKey)
    {
        IEspCommandDeviceSynchronizeInternet action = new EspCommandDeviceSynchronizeInternet();
        return action.doCommandDeviceSynchronizeInternet(userKey);
    }
    
    //本地请求查询设备状态  userKey = null  serverRequired=false   localRequired =true
    private void __doActionDeviceSynchronizeInterentDiscoverLocal(final String userKey, boolean serverRequired,
        boolean localRequired)
    {
        // internet variables
        Callable<List<IEspDevice>> taskInternet = null;
        Future<List<IEspDevice>> futureInternet = null;
        
        
        // local variables
        List<IOTAddress> iotAddressList = new ArrayList<IOTAddress>();
        Callable<List<IOTAddress>> taskLocal = null;
        Future<List<IOTAddress>> futureLocal = null;
        
        // task Internet  serverRequired=false
        if (serverRequired)
        {
            taskInternet = new Callable<List<IEspDevice>>()
            {
                
                @Override
                public List<IEspDevice> call()
                    throws Exception
                {
                    log.debug(Thread.currentThread().toString()
                        + "##__doActionDeviceSynchronizeInterentDiscoverLocal(userKey=[" + userKey
                        + "]): doCommandDeviceSynchronizeInternet()");
                    return doCommandDeviceSynchronizeInternet(userKey);
                }
                
            };
            futureInternet = EspBaseApiUtil.submit(taskInternet);
        }
        
        // task Local
        if (localRequired)
        {
            taskLocal = new Callable<List<IOTAddress>>()
            {
                
                @Override
                public List<IOTAddress> call()
                    throws Exception
                {
                    log.debug(Thread.currentThread().toString()
                        + "##__doActionDeviceSynchronizeInterentDiscoverLocal(userKey=[" + userKey
                        + "]): doCommandDeviceDiscoverLocal()");
                    return doCommandDeviceDiscoverLocal();
                }
                
            };
        }
        
        if (localRequired && serverRequired)
        {
            for (int executeTime = 0; executeTime < UDP_EXECUTE_MAX_TIMES; executeTime++)
            {
                if (executeTime >= UDP_EXECUTE_MIN_TIMES && futureInternet.isDone())
                {
                    break;
                }
                futureLocal = EspBaseApiUtil.submit(taskLocal);
                // get local result
                try
                {
                    List<IOTAddress> localResult = futureLocal.get();
                    for (IOTAddress iotAddress : localResult)
                    {
                        // add iotAddress if iotAddressList doesn't have
                        if (!iotAddressList.contains(iotAddress))
                        {
                            iotAddressList.add(iotAddress);
                        }
                    }
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                catch (ExecutionException e)
                {
                    e.printStackTrace();
                }
            }
        }
        
        // only localRequired, do discover local only once
        
        else if (localRequired)
        {
        	log.error( "START SCAN local gateway");
            futureLocal = EspBaseApiUtil.submit(taskLocal);
        }
        
        if (serverRequired)
        {
            // wait futureInternet finished
            while (!futureInternet.isDone())
            {
                try
                {
                    Thread.sleep(500);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        }
        else if (localRequired)
        {
            // wait local finished  等待扫描结果
            while (!futureLocal.isDone())
            {
                try
                {
                    Thread.sleep(500);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
            try
            {
                // get local result
            	//Waits if necessary for the computation to complete, and then retrieves its result.
            	List<IOTAddress> localResult = futureLocal.get();
            	//log.error("localResult=" +localResult.toString());
                for (IOTAddress iotAddress : localResult)
                {
                    // add iotAddress if iotAddressList doesn't have
                	log.error(iotAddress.toString() + "localResult");
                    if (!iotAddressList.contains(iotAddress))
                    {
                        iotAddressList.add(iotAddress);
                    }
                }
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            catch (ExecutionException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            throw new IllegalArgumentException("serverRequired = false, localRequired = false");
        }
        
        IEspDeviceCache deviceCache = EspDeviceCache.getInstance();
        try
        {
            // add sta device list
            if (localRequired)
            {
                // add the discover local result
                if (iotAddressList.isEmpty())
                {
                    // add EmptyIOTAddress to distinguish from localRequired is false
                    deviceCache.addStaDeviceCache(IOTAddress.EmptyIOTAddress);
                }
                else
                {
                	//正常会在这里把扫描到的网关添加到devicecache里面
                    deviceCache.addStaDeviceCacheList(iotAddressList);
                }
            }
            
            List<IEspDevice> internetResult = null;
            if (serverRequired)
            {
            	//阻塞式读取服务器取回的数据
                internetResult = futureInternet.get();
            }
            
            // Internet unaccessible or serverRequired = false
            if (internetResult == null)
            {
                if (serverRequired)
                {
                    log.error("Internet unaccessible");
                    deviceCache.addLocalDeviceCacheList(iotAddressList);
                }
                else
                {
                    log.error("add EmptyDevice 1");
                    //add EmptyDevice to distinguish from Internet unaccessible
                    deviceCache.addServerLocalDeviceCache(EspDevice.EmptyDevice1);
                }
            }
            // Internet accessible
            else
            {
                if (!internetResult.isEmpty())
                {
                    
                    for (IEspDevice device : internetResult)
                    {
                        for (IOTAddress iotAddress : iotAddressList)
                        {
                            String bssid1 = device.getBssid();
                            String bssid2 = iotAddress.getBSSID();
                            if (bssid1.equals(bssid2))
                            {
                                // deviceState could be isStateOffline() or isStateInternet()
                                IEspDeviceState deviceState = device.getDeviceState();
                                if (deviceState.isStateOffline())
                                {
                                    deviceState.clearStateOffline();
                                }
                                device.setInetAddress(iotAddress.getInetAddress());
                                device.setIsMeshDevice(iotAddress.isMeshDevice());
                                device.setRouter(iotAddress.getRouter());
                                device.setRootDeviceBssid(iotAddress.getRootBssid());
                                deviceState.addStateLocal();
                                break;
                            }
                        }
                    }
                    deviceCache.addServerLocalDeviceCacheList(internetResult);
                }
                else
                {
                    log.error("add EmptyDevice 2");
                    // add EmptyDevice to distinguish from Internet unaccessible
                    deviceCache.addServerLocalDeviceCache(EspDevice.EmptyDevice2);
                }
            }
            deviceCache.notifyIUser(NotifyType.PULL_REFRESH);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        catch (ExecutionException e)
        {
            e.printStackTrace();
        }
    }
    
    @Override
    public void doActionDeviceSynchronizeInterentDiscoverLocal(final String userKey)
    {
        // do it in background thread
        EspBaseApiUtil.submit(new Runnable()
        {
            
            @Override
            public void run()
            {
                __doActionDeviceSynchronizeInterentDiscoverLocal(userKey, true, true);
            }
            
        });
    }
    
    @Override
    public void doActionDeviceSynchronizeDiscoverLocal(boolean isSyn)
    {
        if (isSyn)
        {
            __doActionDeviceSynchronizeInterentDiscoverLocal(null, false, true);
        }
        else
        {
        	//这里把任务提交到线程池进行处理  最后在输出给UI显示   运行就返回，异步处理机制
            EspBaseApiUtil.submit(new Runnable()
            {
                
                @Override
                public void run()
                {
                    __doActionDeviceSynchronizeInterentDiscoverLocal(null, false, true);
                }
                
            });
        }
    }
    
    @Override
    public void doActionDeviceSynchronizeInternet(final String userKey)
    {
        EspBaseApiUtil.submit(new Runnable()
        {
            
            @Override
            public void run()
            {
                __doActionDeviceSynchronizeInterentDiscoverLocal(userKey, true, false);
            }
            
        });
    }
    
}
