package com.espressif.iot.action.device.common;

import java.util.List;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import com.espressif.iot.adt.tree.IEspDeviceTreeElement;
import com.espressif.iot.base.api.EspBaseApiUtil;
import com.espressif.iot.command.IEspCommandInternet;
import com.espressif.iot.command.device.light.EspCommandLightPostStatusInternet;
import com.espressif.iot.command.device.light.IEspCommandLightPostStatusInternet;
import com.espressif.iot.command.device.plug.EspCommandPlugPostStatusInternet;
import com.espressif.iot.command.device.plug.IEspCommandPlugPostStatusInternet;
import com.espressif.iot.command.device.plugs.EspCommandPlugsPostStatusInternet;
import com.espressif.iot.command.device.plugs.IEspCommandPlugsPostStatusInternet;
import com.espressif.iot.command.device.remote.EspCommandRemotePostStatusInternet;
import com.espressif.iot.command.device.remote.IEspCommandRemotePostStatusInternet;
import com.espressif.iot.device.IEspDevice;
import com.espressif.iot.device.IEspDeviceLight;
import com.espressif.iot.device.IEspDevicePlug;
import com.espressif.iot.device.IEspDevicePlugs;
import com.espressif.iot.device.IEspDeviceRemote;
import com.espressif.iot.device.IEspDeviceRoot;
import com.espressif.iot.type.device.EspDeviceType;
import com.espressif.iot.type.device.IEspDeviceStatus;
import com.espressif.iot.type.device.status.IEspStatusLight;
import com.espressif.iot.type.device.status.IEspStatusPlug;
import com.espressif.iot.type.device.status.IEspStatusPlugs;
import com.espressif.iot.type.device.status.IEspStatusPlugs.IAperture;
import com.espressif.iot.type.device.status.IEspStatusRemote;
import com.espressif.iot.type.net.HeaderPair;
import com.espressif.iot.util.RouterUtil;

public class EspActionDevicePostStatusInternet implements IEspActionDevicePostStatusInternet, IEspCommandInternet
{
    
    @Override
    public boolean doActionDevicePostStatusInternet(IEspDevice device, IEspDeviceStatus status)
    {
        return doActionDevicePostStatusInternet(device, status, false);
    }
    
    @Override
    public boolean doActionDevicePostStatusInternet(IEspDevice device, IEspDeviceStatus status, boolean isBroadcast)
    {
        EspDeviceType deviceType = device.getDeviceType();
        String deviceKey = device.getKey();
        String deviceRouter = null;
        if (isBroadcast)
        {
            deviceRouter = device.getRouter();
        }
        boolean suc = false;
        switch (deviceType)
        {
            case FLAMMABLE:
                break;
            case HUMITURE:
                break;
            case VOLTAGE:
                break;
            case LIGHT:
                IEspStatusLight lightStatus = (IEspStatusLight)status;
                IEspCommandLightPostStatusInternet lightCommand = new EspCommandLightPostStatusInternet();
                suc = lightCommand.doCommandLightPostStatusInternet(deviceKey, lightStatus, deviceRouter);
                if (suc)
                {
                    IEspDeviceLight light = (IEspDeviceLight)device;
                    light.getStatusLight().setPeriod(lightStatus.getPeriod());
                    light.getStatusLight().setRed(lightStatus.getRed());
                    light.getStatusLight().setGreen(lightStatus.getGreen());
                    light.getStatusLight().setBlue(lightStatus.getBlue());
                    light.getStatusLight().setCWhite(lightStatus.getCWhite());
                    light.getStatusLight().setWWhite(lightStatus.getWWhite());
                }
                return suc;
            case PLUG:
                IEspStatusPlug plugStatus = (IEspStatusPlug)status;
                IEspCommandPlugPostStatusInternet plugCommand = new EspCommandPlugPostStatusInternet();
                suc = plugCommand.doCommandPlugPostStatusInternet(deviceKey, plugStatus, deviceRouter);
                if (suc)
                {
                    IEspDevicePlug plugDevice = (IEspDevicePlug)device;
                    plugDevice.getStatusPlug().setIsOn(plugStatus.isOn());
                }
                return suc;
            case REMOTE:
                IEspStatusRemote remoteStatus = (IEspStatusRemote)status;
                IEspCommandRemotePostStatusInternet remoteCommand = new EspCommandRemotePostStatusInternet();
                suc = remoteCommand.doCommandRemotePostStatusInternet(deviceKey, remoteStatus, deviceRouter);
                if (suc)
                {
                    IEspDeviceRemote remote = (IEspDeviceRemote)device;
                    remote.getStatusRemote().setAddress(remoteStatus.getAddress());
                    remote.getStatusRemote().setCommand(remoteStatus.getCommand());
                    remote.getStatusRemote().setRepeat(remoteStatus.getRepeat());
                }
                return suc;
            case PLUGS:
                IEspStatusPlugs plugsStatus = (IEspStatusPlugs)status;
                IEspCommandPlugsPostStatusInternet plugsCommand = new EspCommandPlugsPostStatusInternet();
                suc = plugsCommand.doCommandPlugsPostStatusInternet(deviceKey, plugsStatus, deviceRouter);
                if (suc)
                {
                    IEspDevicePlugs plugs = (IEspDevicePlugs)device;
                    for (IAperture postAperture : plugsStatus.getStatusApertureList())
                    {
                        plugs.updateApertureOnOff(postAperture);
                    }
                }
                return suc;
            case ROOT:
                doRootRouterCommandInternet((IEspDeviceRoot)device, status);
                return true;
            case NEW:
                break;
        }
        throw new IllegalArgumentException();
    
    }
    
    private void doRootRouterCommandInternet(IEspDeviceRoot device, IEspDeviceStatus status)
    {
        List<IEspDeviceTreeElement> childList = device.getDeviceTreeElementList();
        for (IEspDeviceTreeElement element : childList)
        {
            if (element.getLevel() == FIRST_CHILD_LEVEL)
            {
                IEspDevice child = element.getCurrentDevice();
                EspBaseApiUtil.submit(new RootStatusRunnable(child, status));
            }
        }
    }
    
    private class RootStatusRunnable implements Runnable
    {
        private IEspDevice device;
        
        private IEspDeviceStatus status;
        
        public RootStatusRunnable(IEspDevice device, IEspDeviceStatus status)
        {
            this.device = device;
            this.status = status;
        }
        
        @Override
        public void run()
        {
            if (status instanceof IEspStatusLight)
            {
                IEspStatusLight lightStatus = (IEspStatusLight)status;
                IEspCommandLightPostStatusInternet lightCommand = new EspCommandLightPostStatusInternet();
                lightCommand.doCommandLightPostStatusInternet(device.getKey(), lightStatus, device.getRouter());
            }
            else if (status instanceof IEspStatusPlug)
            {
                IEspStatusPlug plugStatus = (IEspStatusPlug)status;
                IEspCommandPlugPostStatusInternet plugCommand = new EspCommandPlugPostStatusInternet();
                plugCommand.doCommandPlugPostStatusInternet(device.getKey(), plugStatus, device.getRouter());
            }
            else if (status instanceof IEspStatusRemote)
            {
                IEspStatusRemote remoteStatus = (IEspStatusRemote)status;
                IEspCommandRemotePostStatusInternet remoteCommand = new EspCommandRemotePostStatusInternet();
                remoteCommand.doCommandRemotePostStatusInternet(device.getKey(), remoteStatus, device.getRouter());
            }
        }
    }

	@Override
	public boolean doActionDeviceFactoryInternet(IEspDevice device) {
		// TODO Auto-generated method stub
		 EspDeviceType deviceType = device.getDeviceType();
	        String deviceKey = device.getKey();
	        boolean suc = false;
	        switch (deviceType)
	        {
	            case FLAMMABLE:
	                break;
	            case HUMITURE:
	                break;
	            case VOLTAGE:
	                break;
	            case LIGHT:
	               
	            case PLUG:
	               
	            case PLUGS:
	            	return postFactoryStatus(deviceKey);
	            case ROOT:
	               
	            case NEW:
	                break;
	        }
	        throw new IllegalArgumentException();
	}
	private boolean postFactoryStatus(String deviceKey)
    {
		JSONObject result = null;
		String URL = "https://iot.espressif.cn/v1/device/rpc/?deliver_to_device=true&action=req_factory";
        
        String headerKey = Authorization;
        String headerValue = Token + " " + deviceKey;
        HeaderPair header = new HeaderPair(headerKey, headerValue);
        
        String url = URL;
        
        result = EspBaseApiUtil.Get(url,  header);
        int status = -1;
        try
        {
            if (result != null)
                status = Integer.parseInt(result.getString(Status));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        if (status == HttpStatus.SC_OK)
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }

}
