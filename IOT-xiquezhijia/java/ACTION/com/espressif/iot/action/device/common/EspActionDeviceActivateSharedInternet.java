package com.espressif.iot.action.device.common;

import android.text.TextUtils;

import com.espressif.iot.command.device.New.EspCommandDeviceNewActivateInternet;
import com.espressif.iot.command.device.New.EspCommandDeviceNewActivateLocal;
import com.espressif.iot.command.device.New.IEspCommandDeviceNewActivateInternet;
import com.espressif.iot.command.device.New.IEspCommandDeviceNewActiveLocal;
import com.espressif.iot.device.IEspDevice;
import com.espressif.iot.device.cache.IEspDeviceCache.NotifyType;
import com.espressif.iot.model.device.cache.EspDeviceCache;
import com.espressif.iot.type.device.EspDeviceType;

public class EspActionDeviceActivateSharedInternet implements IEspActionDeviceActivateSharedInternet
{
    
    @Override
    public boolean doActionDeviceActivateSharedInternet(long userId, String userKey, String sharedDeviceKey)
    {
    	 IEspDevice device = null;
    	if(userId >0 && TextUtils.isEmpty(userKey))
        	{
    			IEspCommandDeviceNewActivateInternet command = new EspCommandDeviceNewActivateInternet();
    			device = command.doCommandNewActivateInternet(userId, userKey, sharedDeviceKey);
        	}
        else
        	{
        	EspDeviceType type = EspDeviceType.getDevicetypte(sharedDeviceKey);
        	IEspCommandDeviceNewActiveLocal xiquecommandlocal = new EspCommandDeviceNewActivateLocal();
			
        	if(type == EspDeviceType.GATEWAY)
        		{
        			device = xiquecommandlocal.doCommandNewActiveLocal(sharedDeviceKey);
        		}else if(type != EspDeviceType.GATEWAY && EspDeviceType.XQchildDeviceisvalid(type)){
        			//device = xiquecommandlocal.doCommandNewChildActiveLocal(sharedDeviceKey, gateway)
        		}else if(type == EspDeviceType.WIFICAMERA){
        			
        		}
        	}
        if (device != null) {
            EspDeviceCache.getInstance().addSharedDeviceCache(device);
            //广播一个命令去读取配置成功的设备，然后显示在界面上
            EspDeviceCache.getInstance().notifyIUser(NotifyType.STATE_MACHINE_UI);
            return true;
        } else {
            return false;
        }
    }

	
    
}
